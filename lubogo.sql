-- MySQL dump 10.13  Distrib 8.0.21, for Linux (x86_64)
--
-- Host: localhost    Database: lubogo
-- ------------------------------------------------------
-- Server version	8.0.21-0ubuntu0.20.04.4

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `appversion`
--

DROP TABLE IF EXISTS `appversion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `appversion` (
  `idAppVersion` int NOT NULL AUTO_INCREMENT,
  `version` varchar(250) DEFAULT NULL,
  `descripcion` varchar(250) DEFAULT NULL,
  `fechaAlta` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`idAppVersion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appversion`
--

LOCK TABLES `appversion` WRITE;
/*!40000 ALTER TABLE `appversion` DISABLE KEYS */;
/*!40000 ALTER TABLE `appversion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cancelarpedido`
--

DROP TABLE IF EXISTS `cancelarpedido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cancelarpedido` (
  `idCancelarPedido` int NOT NULL AUTO_INCREMENT,
  `Fecha` datetime DEFAULT NULL,
  `Descripcion` varchar(45) DEFAULT NULL,
  `idUsuario` int NOT NULL,
  `idRepartidor` int NOT NULL,
  `idViaje` int NOT NULL,
  PRIMARY KEY (`idCancelarPedido`),
  KEY `fk_CancelarPedido_Persona1_idx` (`idUsuario`),
  KEY `fk_CancelarPedido_Persona2_idx` (`idRepartidor`),
  KEY `fk_CancelarPedido_Viaje1_idx` (`idViaje`),
  CONSTRAINT `fk_CancelarPedido_Persona1` FOREIGN KEY (`idUsuario`) REFERENCES `persona` (`idUsuario`),
  CONSTRAINT `fk_CancelarPedido_Persona2` FOREIGN KEY (`idRepartidor`) REFERENCES `persona` (`idUsuario`),
  CONSTRAINT `fk_CancelarPedido_Viaje1` FOREIGN KEY (`idViaje`) REFERENCES `viaje` (`idViaje`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cancelarpedido`
--

LOCK TABLES `cancelarpedido` WRITE;
/*!40000 ALTER TABLE `cancelarpedido` DISABLE KEYS */;
/*!40000 ALTER TABLE `cancelarpedido` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `carrito`
--

DROP TABLE IF EXISTS `carrito`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `carrito` (
  `IdCarrito` int NOT NULL AUTO_INCREMENT,
  `idUsuario` int NOT NULL,
  `Status` int NOT NULL,
  `CantidadProd` double NOT NULL,
  `TotalCompra` double NOT NULL,
  `PropinaCond` double NOT NULL,
  PRIMARY KEY (`IdCarrito`),
  KEY `fk_Producto_has_Persona_Persona1_idx` (`idUsuario`),
  CONSTRAINT `fk_Producto_has_Persona_Persona1` FOREIGN KEY (`idUsuario`) REFERENCES `persona` (`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carrito`
--

LOCK TABLES `carrito` WRITE;
/*!40000 ALTER TABLE `carrito` DISABLE KEYS */;
/*!40000 ALTER TABLE `carrito` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ciudad`
--

DROP TABLE IF EXISTS `ciudad`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ciudad` (
  `idCiudad` int NOT NULL AUTO_INCREMENT,
  `Ciudad` varchar(100) NOT NULL,
  `Latitud` varchar(50) DEFAULT NULL,
  `Longitud` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idCiudad`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ciudad`
--

LOCK TABLES `ciudad` WRITE;
/*!40000 ALTER TABLE `ciudad` DISABLE KEYS */;
INSERT INTO `ciudad` VALUES (1,'AdminGeneral',NULL,NULL),(2,'Huauchinango',NULL,NULL),(3,'Xcotepec',NULL,NULL);
/*!40000 ALTER TABLE `ciudad` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `codigoRecuperarPassword`
--

DROP TABLE IF EXISTS `codigoRecuperarPassword`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `codigoRecuperarPassword` (
  `idCodigoRecuperarPassword` int NOT NULL AUTO_INCREMENT,
  `Codigo` varchar(6) DEFAULT NULL,
  `FechaAlta` datetime DEFAULT NULL,
  `FechaExpiracion` datetime DEFAULT NULL,
  `Status` tinyint DEFAULT NULL,
  `idUsuario` int NOT NULL,
  PRIMARY KEY (`idCodigoRecuperarPassword`),
  KEY `fk_recuperarPassword_persona1` (`idUsuario`),
  CONSTRAINT `fk_recuperarPassword_persona1` FOREIGN KEY (`idUsuario`) REFERENCES `persona` (`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `codigoRecuperarPassword`
--

LOCK TABLES `codigoRecuperarPassword` WRITE;
/*!40000 ALTER TABLE `codigoRecuperarPassword` DISABLE KEYS */;
/*!40000 ALTER TABLE `codigoRecuperarPassword` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `conekta`
--

DROP TABLE IF EXISTS `conekta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `conekta` (
  `idConekta` int NOT NULL AUTO_INCREMENT,
  `idClienteConekta` varchar(250) DEFAULT NULL,
  `idUsuario` int NOT NULL,
  PRIMARY KEY (`idConekta`),
  KEY `fk_conekta_persona1_idx` (`idUsuario`),
  CONSTRAINT `fk_conekta_persona1` FOREIGN KEY (`idUsuario`) REFERENCES `persona` (`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `conekta`
--

LOCK TABLES `conekta` WRITE;
/*!40000 ALTER TABLE `conekta` DISABLE KEYS */;
/*!40000 ALTER TABLE `conekta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `datosbancarios`
--

DROP TABLE IF EXISTS `datosbancarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `datosbancarios` (
  `iddatosbancarios` int NOT NULL AUTO_INCREMENT,
  `nombreBanco` varchar(45) DEFAULT NULL,
  `numCuenta` varchar(45) DEFAULT NULL,
  `numTarjeta` varchar(45) DEFAULT NULL,
  `claveInterBancaria` varchar(45) DEFAULT NULL,
  `fechaAlta` datetime DEFAULT CURRENT_TIMESTAMP,
  `idUsuario` int NOT NULL,
  PRIMARY KEY (`iddatosbancarios`),
  KEY `fk_datosbancarios_persona1_idx` (`idUsuario`),
  CONSTRAINT `fk_datosbancarios_persona1` FOREIGN KEY (`idUsuario`) REFERENCES `persona` (`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `datosbancarios`
--

LOCK TABLES `datosbancarios` WRITE;
/*!40000 ALTER TABLE `datosbancarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `datosbancarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detallecarrito`
--

DROP TABLE IF EXISTS `detallecarrito`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `detallecarrito` (
  `idDetalleCarrito` int NOT NULL AUTO_INCREMENT,
  `CantidadProd` double NOT NULL,
  `TotalProd` double NOT NULL,
  `idCarrito` int NOT NULL,
  `idProducto` int NOT NULL,
  PRIMARY KEY (`idDetalleCarrito`),
  KEY `fk_DetalleCarrito_Carrito1_idx` (`idCarrito`),
  KEY `fk_DetalleCarrito_Producto1_idx` (`idProducto`),
  CONSTRAINT `fk_DetalleCarrito_Carrito1` FOREIGN KEY (`idCarrito`) REFERENCES `carrito` (`IdCarrito`),
  CONSTRAINT `fk_DetalleCarrito_Producto1` FOREIGN KEY (`idProducto`) REFERENCES `producto` (`idProducto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detallecarrito`
--

LOCK TABLES `detallecarrito` WRITE;
/*!40000 ALTER TABLE `detallecarrito` DISABLE KEYS */;
/*!40000 ALTER TABLE `detallecarrito` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dias`
--

DROP TABLE IF EXISTS `dias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `dias` (
  `iddias` int NOT NULL,
  `descripcion` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`iddias`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dias`
--

LOCK TABLES `dias` WRITE;
/*!40000 ALTER TABLE `dias` DISABLE KEYS */;
/*!40000 ALTER TABLE `dias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `direccion`
--

DROP TABLE IF EXISTS `direccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `direccion` (
  `idDireccion` int NOT NULL AUTO_INCREMENT,
  `Calle` varchar(250) DEFAULT NULL,
  `numeroExterior` varchar(5) DEFAULT NULL,
  `numeroInterior` varchar(5) DEFAULT NULL,
  `Colonia` varchar(250) DEFAULT NULL,
  `Municipio` varchar(250) DEFAULT NULL,
  `Ciudad` varchar(250) DEFAULT NULL,
  `Latitud` varchar(250) DEFAULT NULL,
  `Longitud` varchar(250) DEFAULT NULL,
  `Estado` varchar(250) DEFAULT NULL,
  `CodPostal` varchar(10) DEFAULT NULL,
  `Guardado` int DEFAULT '0',
  `idEstablecimiento` int DEFAULT NULL,
  `idUsuario` int DEFAULT NULL,
  `status` tinyint DEFAULT '1',
  PRIMARY KEY (`idDireccion`),
  KEY `fk_direccion_establecimiento1_idx` (`idEstablecimiento`),
  KEY `fk_direccion_persona1_idx` (`idUsuario`),
  CONSTRAINT `fk_direccion_establecimiento1` FOREIGN KEY (`idEstablecimiento`) REFERENCES `establecimiento` (`idEstablecimiento`),
  CONSTRAINT `fk_direccion_persona1` FOREIGN KEY (`idUsuario`) REFERENCES `persona` (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `direccion`
--

LOCK TABLES `direccion` WRITE;
/*!40000 ALTER TABLE `direccion` DISABLE KEYS */;
INSERT INTO `direccion` VALUES (1,'Benito Jurez','19','','Patoltecoya','Huauchinango','Huauchinango','20.209043','-98.033196','Puebla','73165',1,NULL,3,1),(2,'Benito Jurez','19','2','Patoltecoya','Huauchinango','Huauchinango','20.209043','-98.033196','Puebla','73165',1,NULL,3,0),(3,'Benito Jurez','19','','Patoltecoya','Huauchinango','Huauchinango','20.209043','-98.033196','Puebla','73165',1,4,NULL,1);
/*!40000 ALTER TABLE `direccion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documentosvehiculo`
--

DROP TABLE IF EXISTS `documentosvehiculo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `documentosvehiculo` (
  `idDocVehiculo` int NOT NULL,
  `Documento` varchar(150) NOT NULL,
  `Status` int NOT NULL,
  `FechaAlta` datetime NOT NULL,
  `FechaExpiracion` datetime NOT NULL,
  `idTipoDocumento` int NOT NULL,
  `idUsuario` int NOT NULL,
  PRIMARY KEY (`idDocVehiculo`),
  KEY `fk_Documentos_Vehiculo_Tipo_Documento1_idx` (`idTipoDocumento`),
  KEY `fk_Documentos_Vehiculo_Persona1_idx` (`idUsuario`),
  CONSTRAINT `fk_Documentos_Vehiculo_Persona1` FOREIGN KEY (`idUsuario`) REFERENCES `persona` (`idUsuario`),
  CONSTRAINT `fk_Documentos_Vehiculo_Tipo_Documento1` FOREIGN KEY (`idTipoDocumento`) REFERENCES `tipo_documento` (`idTipoDocumento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documentosvehiculo`
--

LOCK TABLES `documentosvehiculo` WRITE;
/*!40000 ALTER TABLE `documentosvehiculo` DISABLE KEYS */;
/*!40000 ALTER TABLE `documentosvehiculo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `establecimiento`
--

DROP TABLE IF EXISTS `establecimiento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `establecimiento` (
  `idEstablecimiento` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(250) DEFAULT NULL,
  `urlFoto` varchar(250) DEFAULT '',
  `email` varchar(250) DEFAULT NULL,
  `telefono` varchar(15) DEFAULT NULL,
  `idTipoEstablecimiento` int DEFAULT NULL,
  `idStatusEstablecimiento` int DEFAULT '1',
  `etiquetas` varchar(250) DEFAULT NULL,
  `calificacion` int DEFAULT '5',
  `idAdministrador` int DEFAULT NULL,
  `urlPortada` varchar(250) DEFAULT '',
  PRIMARY KEY (`idEstablecimiento`),
  KEY `fk_Establecimiento_TipoEstablecimiento1_idx` (`idTipoEstablecimiento`),
  KEY `fk_establecimiento_statusEstablecimiento1_idx` (`idStatusEstablecimiento`),
  KEY `fk_establecimiento_persona1_idx` (`idAdministrador`),
  CONSTRAINT `fk_establecimiento_persona1` FOREIGN KEY (`idAdministrador`) REFERENCES `persona` (`idUsuario`),
  CONSTRAINT `fk_establecimiento_statusEstablecimiento1` FOREIGN KEY (`idStatusEstablecimiento`) REFERENCES `statusestablecimiento` (`idstatusEstablecimiento`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `fk_Establecimiento_TipoEstablecimiento1` FOREIGN KEY (`idTipoEstablecimiento`) REFERENCES `tipoestablecimiento` (`idTipoEstablecimiento`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `establecimiento`
--

LOCK TABLES `establecimiento` WRITE;
/*!40000 ALTER TABLE `establecimiento` DISABLE KEYS */;
INSERT INTO `establecimiento` VALUES (4,'tres gatos','','tres_gatos@gmail.com','7761052213',7,1,'tacos,tres,gatos,los tres gatos',5,4,''),(5,'Cafe pahuatlan','','cafe@gmail.com','7761052215',7,1,'cafe,pahuatlan,te,cafe pahuatlan',5,6,''),(6,'pizza Napoli','','pizza_napoli@gmail.com','7761052289',7,1,'pizza,napoli,pizza napoli, italiana, comida italiana',5,5,'');
/*!40000 ALTER TABLE `establecimiento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `establecimiento_has_subcategorias`
--

DROP TABLE IF EXISTS `establecimiento_has_subcategorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `establecimiento_has_subcategorias` (
  `idEstablecimiento` int NOT NULL,
  `idSubCategorias` int NOT NULL,
  PRIMARY KEY (`idEstablecimiento`,`idSubCategorias`),
  KEY `fk_Establecimiento_has_SubCategorias_SubCategorias1_idx` (`idSubCategorias`),
  KEY `fk_Establecimiento_has_SubCategorias_Establecimiento1_idx` (`idEstablecimiento`),
  CONSTRAINT `fk_Establecimiento_has_SubCategorias_Establecimiento1` FOREIGN KEY (`idEstablecimiento`) REFERENCES `establecimiento` (`idEstablecimiento`),
  CONSTRAINT `fk_Establecimiento_has_SubCategorias_SubCategorias1` FOREIGN KEY (`idSubCategorias`) REFERENCES `subcategorias` (`idSubCategorias`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `establecimiento_has_subcategorias`
--

LOCK TABLES `establecimiento_has_subcategorias` WRITE;
/*!40000 ALTER TABLE `establecimiento_has_subcategorias` DISABLE KEYS */;
INSERT INTO `establecimiento_has_subcategorias` VALUES (4,2),(5,2),(6,5),(4,10),(5,10),(5,11);
/*!40000 ALTER TABLE `establecimiento_has_subcategorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `factura`
--

DROP TABLE IF EXISTS `factura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `factura` (
  `idFactura` int NOT NULL AUTO_INCREMENT,
  `idFacturama` varchar(250) DEFAULT NULL,
  `idViaje` int NOT NULL,
  PRIMARY KEY (`idFactura`),
  KEY `fk_factura_viaje1_idx` (`idViaje`),
  CONSTRAINT `fk_factura_viaje1` FOREIGN KEY (`idViaje`) REFERENCES `viaje` (`idViaje`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `factura`
--

LOCK TABLES `factura` WRITE;
/*!40000 ALTER TABLE `factura` DISABLE KEYS */;
/*!40000 ALTER TABLE `factura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fotovehiculo`
--

DROP TABLE IF EXISTS `fotovehiculo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fotovehiculo` (
  `idFotoVehiculo` int NOT NULL AUTO_INCREMENT,
  `Imagen` varchar(250) NOT NULL,
  `FechaAlta` datetime NOT NULL,
  `FechaExpiracion` datetime NOT NULL,
  `idVehiculo` int NOT NULL,
  PRIMARY KEY (`idFotoVehiculo`),
  KEY `fk_FotoVehiculo_Vehiculo1_idx` (`idVehiculo`),
  CONSTRAINT `fk_FotoVehiculo_Vehiculo1` FOREIGN KEY (`idVehiculo`) REFERENCES `vehiculo` (`idVehiculo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fotovehiculo`
--

LOCK TABLES `fotovehiculo` WRITE;
/*!40000 ALTER TABLE `fotovehiculo` DISABLE KEYS */;
/*!40000 ALTER TABLE `fotovehiculo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guardado`
--

DROP TABLE IF EXISTS `guardado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `guardado` (
  `idUsuario` int NOT NULL,
  `idProducto` int NOT NULL,
  `idDireccion` int NOT NULL,
  KEY `fk_Usuario_has_Producto_Producto1_idx` (`idProducto`),
  KEY `fk_Usuario_has_Producto_Usuario_idx` (`idUsuario`),
  KEY `fk_guardado_direccion1_idx` (`idDireccion`),
  CONSTRAINT `fk_guardado_direccion1` FOREIGN KEY (`idDireccion`) REFERENCES `direccion` (`idDireccion`),
  CONSTRAINT `fk_Usuario_has_Producto_Producto1` FOREIGN KEY (`idProducto`) REFERENCES `producto` (`idProducto`),
  CONSTRAINT `fk_Usuario_has_Producto_Usuario` FOREIGN KEY (`idUsuario`) REFERENCES `persona` (`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guardado`
--

LOCK TABLES `guardado` WRITE;
/*!40000 ALTER TABLE `guardado` DISABLE KEYS */;
/*!40000 ALTER TABLE `guardado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `horario`
--

DROP TABLE IF EXISTS `horario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `horario` (
  `idhorario` int NOT NULL,
  `idEstablecimiento` int DEFAULT NULL,
  `iddias` int DEFAULT NULL,
  `horaApertura` time DEFAULT NULL,
  `horaCierre` time DEFAULT NULL,
  PRIMARY KEY (`idhorario`),
  KEY `fk_horario_establecimiento1_idx` (`idEstablecimiento`),
  KEY `fk_horario_dias1_idx` (`iddias`),
  CONSTRAINT `fk_horario_dias1` FOREIGN KEY (`iddias`) REFERENCES `dias` (`iddias`),
  CONSTRAINT `fk_horario_establecimiento1` FOREIGN KEY (`idEstablecimiento`) REFERENCES `establecimiento` (`idEstablecimiento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `horario`
--

LOCK TABLES `horario` WRITE;
/*!40000 ALTER TABLE `horario` DISABLE KEYS */;
/*!40000 ALTER TABLE `horario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `metodopago`
--

DROP TABLE IF EXISTS `metodopago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `metodopago` (
  `idMetodoPago` int NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(200) NOT NULL,
  PRIMARY KEY (`idMetodoPago`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `metodopago`
--

LOCK TABLES `metodopago` WRITE;
/*!40000 ALTER TABLE `metodopago` DISABLE KEYS */;
/*!40000 ALTER TABLE `metodopago` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfilfacturacion`
--

DROP TABLE IF EXISTS `perfilfacturacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `perfilfacturacion` (
  `idperfilfacturacion` int NOT NULL AUTO_INCREMENT,
  `rfc` varchar(250) DEFAULT NULL,
  `razon` varchar(250) DEFAULT NULL,
  `regimenFiscal` varchar(250) DEFAULT NULL,
  `descripcion` varchar(250) DEFAULT NULL,
  `idUsuario` int NOT NULL,
  PRIMARY KEY (`idperfilfacturacion`),
  KEY `fk_perfilfacturacion_persona1_idx` (`idUsuario`),
  CONSTRAINT `fk_perfilfacturacion_persona1` FOREIGN KEY (`idUsuario`) REFERENCES `persona` (`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfilfacturacion`
--

LOCK TABLES `perfilfacturacion` WRITE;
/*!40000 ALTER TABLE `perfilfacturacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `perfilfacturacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `perfilimpuestos`
--

DROP TABLE IF EXISTS `perfilimpuestos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `perfilimpuestos` (
  `idperfilimpuestos` int NOT NULL AUTO_INCREMENT,
  `nombreLegal` varchar(250) DEFAULT NULL,
  `rfc` varchar(250) DEFAULT NULL,
  `status` tinyint DEFAULT NULL,
  `tipoFacturacion` varchar(45) DEFAULT NULL,
  `idtarjeta` int DEFAULT NULL,
  `idMetodoPago` int DEFAULT NULL,
  `idDireccion` int DEFAULT NULL,
  `idUsuario` int NOT NULL,
  PRIMARY KEY (`idperfilimpuestos`),
  KEY `fk_perfilimpuestos_direccion1` (`idDireccion`),
  KEY `fk_perfilimpuestos_metodopago1` (`idMetodoPago`),
  KEY `fk_perfilimpuestos_persona1` (`idUsuario`),
  KEY `fk_perfilimpuestos_tarjeta1` (`idtarjeta`),
  CONSTRAINT `fk_perfilimpuestos_direccion1` FOREIGN KEY (`idDireccion`) REFERENCES `direccion` (`idDireccion`),
  CONSTRAINT `fk_perfilimpuestos_metodopago1` FOREIGN KEY (`idMetodoPago`) REFERENCES `metodopago` (`idMetodoPago`),
  CONSTRAINT `fk_perfilimpuestos_persona1` FOREIGN KEY (`idUsuario`) REFERENCES `persona` (`idUsuario`),
  CONSTRAINT `fk_perfilimpuestos_tarjeta1` FOREIGN KEY (`idtarjeta`) REFERENCES `tarjeta` (`idtarjeta`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `perfilimpuestos`
--

LOCK TABLES `perfilimpuestos` WRITE;
/*!40000 ALTER TABLE `perfilimpuestos` DISABLE KEYS */;
/*!40000 ALTER TABLE `perfilimpuestos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persona`
--

DROP TABLE IF EXISTS `persona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `persona` (
  `idUsuario` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `apellidos` varchar(50) NOT NULL,
  `email` varchar(45) NOT NULL,
  `password` varchar(150) NOT NULL,
  `sexo` varchar(15) DEFAULT NULL,
  `urlFoto` varchar(250) DEFAULT NULL,
  `codigoPais` varchar(45) DEFAULT NULL,
  `telefono` varchar(15) NOT NULL,
  `FechaRegistro` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `idTipoUsuario` int NOT NULL,
  `idStatusPersona` int NOT NULL DEFAULT '1',
  `tycos` tinyint DEFAULT NULL,
  `ocupado` tinyint DEFAULT '0',
  `idCiudad` int DEFAULT NULL,
  PRIMARY KEY (`idUsuario`),
  KEY `fk_Persona_Tipo_Usuario1` (`idTipoUsuario`),
  KEY `fk_persona_statusPersona1` (`idStatusPersona`),
  KEY `fk_persona_ciudad1` (`idCiudad`),
  CONSTRAINT `fk_persona_ciudad1` FOREIGN KEY (`idCiudad`) REFERENCES `ciudad` (`idCiudad`),
  CONSTRAINT `fk_persona_statusPersona1` FOREIGN KEY (`idStatusPersona`) REFERENCES `statuspersona` (`idstatusPersona`),
  CONSTRAINT `fk_Persona_Tipo_Usuario1` FOREIGN KEY (`idTipoUsuario`) REFERENCES `tipousuario` (`idTipoUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persona`
--

LOCK TABLES `persona` WRITE;
/*!40000 ALTER TABLE `persona` DISABLE KEYS */;
INSERT INTO `persona` VALUES (1,'Humbert','Cuautenco','humbert@stardust.com.mx','$6$rounds=5000$@startDustH00l10$CMoTCSgTE.cxQpmgSP3FpobYOyvZsM.lciZp5vSZNRbg30H04lvfx.oR3n9mO6N6zBSZjCsfdft0IFLSxtjqh0','H',NULL,'52','7761052213','2020-05-28 12:36:26',5,1,1,0,1),(2,'Humbert','Cuautenco','humbert@stardust.com.mx','$6$rounds=5000$@startDustH00l10$CMoTCSgTE.cxQpmgSP3FpobYOyvZsM.lciZp5vSZNRbg30H04lvfx.oR3n9mO6N6zBSZjCsfdft0IFLSxtjqh0','H',NULL,'52','7761052213','2020-05-29 15:25:55',1,1,1,0,NULL),(3,'Humbert','Cuautenco','humbert@stardust.com.mx','$6$rounds=5000$@startDustH00l10$CMoTCSgTE.cxQpmgSP3FpobYOyvZsM.lciZp5vSZNRbg30H04lvfx.oR3n9mO6N6zBSZjCsfdft0IFLSxtjqh0','H',NULL,'52','7761052213','2020-05-29 15:44:20',2,2,1,0,2),(4,'Humbert','Cuautenco','humbert@stardust.com.mx','$6$rounds=5000$@startDustH00l10$CMoTCSgTE.cxQpmgSP3FpobYOyvZsM.lciZp5vSZNRbg30H04lvfx.oR3n9mO6N6zBSZjCsfdft0IFLSxtjqh0','H',NULL,'52','7761052213','2020-05-29 15:44:20',3,1,1,0,2),(5,'Humbert','Cuautenco','user_pizza@gmail.com','$6$rounds=5000$@startDustH00l10$CMoTCSgTE.cxQpmgSP3FpobYOyvZsM.lciZp5vSZNRbg30H04lvfx.oR3n9mO6N6zBSZjCsfdft0IFLSxtjqh0','H',NULL,'52','7761052214','2020-05-29 15:44:20',3,1,1,0,2),(6,'Humbert','Cuautenco','user_cafe@gmail.com','$6$rounds=5000$@startDustH00l10$CMoTCSgTE.cxQpmgSP3FpobYOyvZsM.lciZp5vSZNRbg30H04lvfx.oR3n9mO6N6zBSZjCsfdft0IFLSxtjqh0','H',NULL,'52','7761052215','2020-05-29 15:44:20',3,1,1,0,2);
/*!40000 ALTER TABLE `persona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `producto`
--

DROP TABLE IF EXISTS `producto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `producto` (
  `idProducto` int NOT NULL AUTO_INCREMENT,
  `producto` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `codigoProducto` varchar(250) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
  `descripcion` varchar(250) DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `urlFoto` varchar(250) DEFAULT NULL,
  `descuento` double DEFAULT NULL,
  `stock` double DEFAULT NULL,
  `idStatusProducto` int NOT NULL,
  `idEstablecimiento` int NOT NULL,
  `oferta` tinyint DEFAULT NULL,
  `fechaExpiracionOferta` date DEFAULT NULL,
  PRIMARY KEY (`idProducto`),
  KEY `fk_producto_establecimiento1` (`idEstablecimiento`),
  KEY `fk_producto_statusproducto1` (`idStatusProducto`),
  CONSTRAINT `fk_producto_establecimiento1` FOREIGN KEY (`idEstablecimiento`) REFERENCES `establecimiento` (`idEstablecimiento`),
  CONSTRAINT `fk_producto_statusproducto1` FOREIGN KEY (`idStatusProducto`) REFERENCES `statusproducto` (`idstatusproducto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `producto`
--

LOCK TABLES `producto` WRITE;
/*!40000 ALTER TABLE `producto` DISABLE KEYS */;
/*!40000 ALTER TABLE `producto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rechazado`
--

DROP TABLE IF EXISTS `rechazado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rechazado` (
  `idRechazado` int NOT NULL AUTO_INCREMENT,
  `Fecha` datetime NOT NULL,
  `idViaje` int NOT NULL,
  PRIMARY KEY (`idRechazado`),
  KEY `fk_Rechazado_Viaje2` (`idViaje`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rechazado`
--

LOCK TABLES `rechazado` WRITE;
/*!40000 ALTER TABLE `rechazado` DISABLE KEYS */;
/*!40000 ALTER TABLE `rechazado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `smsvalidacion`
--

DROP TABLE IF EXISTS `smsvalidacion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `smsvalidacion` (
  `idSmsValidacion` int NOT NULL AUTO_INCREMENT,
  `codigoPais` varchar(5) DEFAULT NULL,
  `telefono` varchar(45) DEFAULT NULL,
  `codigo` varchar(45) DEFAULT NULL,
  `fechaAlta` datetime DEFAULT CURRENT_TIMESTAMP,
  `fechaExpiracion` datetime DEFAULT NULL,
  `status` tinyint DEFAULT '1',
  PRIMARY KEY (`idSmsValidacion`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `smsvalidacion`
--

LOCK TABLES `smsvalidacion` WRITE;
/*!40000 ALTER TABLE `smsvalidacion` DISABLE KEYS */;
INSERT INTO `smsvalidacion` VALUES (5,'52','7761052213','2899','2020-05-28 12:26:12','2020-05-28 13:26:12',0),(6,'52','7761052213','0536','2020-05-28 21:44:51','2020-05-28 22:44:51',1),(7,'52','7761052213','8162','2020-06-09 12:02:58','2020-06-09 13:02:58',1),(8,'52','7761052213','6287','2020-06-09 12:03:54','2020-06-09 13:03:54',1),(9,'52','7761052213','5707','2020-06-09 12:04:58','2020-06-09 13:04:58',1);
/*!40000 ALTER TABLE `smsvalidacion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statusestablecimiento`
--

DROP TABLE IF EXISTS `statusestablecimiento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `statusestablecimiento` (
  `idstatusEstablecimiento` int NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(100) NOT NULL,
  PRIMARY KEY (`idstatusEstablecimiento`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statusestablecimiento`
--

LOCK TABLES `statusestablecimiento` WRITE;
/*!40000 ALTER TABLE `statusestablecimiento` DISABLE KEYS */;
INSERT INTO `statusestablecimiento` VALUES (1,'Activo'),(2,'Eliminado');
/*!40000 ALTER TABLE `statusestablecimiento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statuspersona`
--

DROP TABLE IF EXISTS `statuspersona`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `statuspersona` (
  `idstatusPersona` int NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  PRIMARY KEY (`idstatusPersona`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statuspersona`
--

LOCK TABLES `statuspersona` WRITE;
/*!40000 ALTER TABLE `statuspersona` DISABLE KEYS */;
INSERT INTO `statuspersona` VALUES (0,'Inactivo'),(1,'Activo'),(2,'Eliminado');
/*!40000 ALTER TABLE `statuspersona` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statusproducto`
--

DROP TABLE IF EXISTS `statusproducto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `statusproducto` (
  `idstatusproducto` int NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(200) NOT NULL,
  PRIMARY KEY (`idstatusproducto`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statusproducto`
--

LOCK TABLES `statusproducto` WRITE;
/*!40000 ALTER TABLE `statusproducto` DISABLE KEYS */;
INSERT INTO `statusproducto` VALUES (1,'Activo'),(2,'Eliminado');
/*!40000 ALTER TABLE `statusproducto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statustransaccion`
--

DROP TABLE IF EXISTS `statustransaccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `statustransaccion` (
  `idStatusTransaccion` int NOT NULL,
  `Descripcion` varchar(200) NOT NULL,
  PRIMARY KEY (`idStatusTransaccion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statustransaccion`
--

LOCK TABLES `statustransaccion` WRITE;
/*!40000 ALTER TABLE `statustransaccion` DISABLE KEYS */;
INSERT INTO `statustransaccion` VALUES (0,'Declinado'),(1,'Pago Pendiente'),(2,'Pagado'),(3,'Activa'),(4,'Expirado'),(5,'Reintegrado'),(6,'Parcialmente retornado'),(7,'Pago Retornado'),(8,'Tiempo de espera de captura expirado');
/*!40000 ALTER TABLE `statustransaccion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `statusviaje`
--

DROP TABLE IF EXISTS `statusviaje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `statusviaje` (
  `idStatusViaje` int NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(200) NOT NULL,
  PRIMARY KEY (`idStatusViaje`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `statusviaje`
--

LOCK TABLES `statusviaje` WRITE;
/*!40000 ALTER TABLE `statusviaje` DISABLE KEYS */;
INSERT INTO `statusviaje` VALUES (1,'Levantada'),(2,'En Proceso'),(3,'Lista'),(4,'En Camino'),(5,'En Destino'),(6,'Entregada'),(7,'Cancelado por usuario'),(8,'Rechazado'),(9,'No se encontró repartidor');
/*!40000 ALTER TABLE `statusviaje` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `subcategorias`
--

DROP TABLE IF EXISTS `subcategorias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `subcategorias` (
  `idSubCategorias` int NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(200) NOT NULL,
  `idTipoEstablecimiento` int NOT NULL,
  `urlImagen` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`idSubCategorias`),
  KEY `fk_subcategorias_tipoestablecimiento1_idx` (`idTipoEstablecimiento`),
  CONSTRAINT `fk_subcategorias_tipoestablecimiento1` FOREIGN KEY (`idTipoEstablecimiento`) REFERENCES `tipoestablecimiento` (`idTipoEstablecimiento`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `subcategorias`
--

LOCK TABLES `subcategorias` WRITE;
/*!40000 ALTER TABLE `subcategorias` DISABLE KEYS */;
INSERT INTO `subcategorias` VALUES (1,'Asiatica',7,'Asiatica.png'),(2,'Cafe',7,'Cafe.png'),(3,'Comida Rapida',7,'Comida_rapida.png'),(4,'Desayunos',7,'Desayuno.png'),(5,'Italiana',7,'Italiana.png'),(6,'Jugos y Licuados',7,'Jugos_licuados.png'),(7,'Mexicana',7,'Mexicana.png'),(8,'Panaderia',7,'Panaderia.png'),(9,'Pollos',7,'Pollos.png'),(10,'Postres',7,'Postres.png'),(11,'Tacos',7,'Tacos.png'),(12,'Vegetariana',7,'Vegetariana.png');
/*!40000 ALTER TABLE `subcategorias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tarifa`
--

DROP TABLE IF EXISTS `tarifa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tarifa` (
  `idTarifa` int NOT NULL AUTO_INCREMENT,
  `TarifaBase` double DEFAULT NULL,
  `Impuestos` double DEFAULT NULL,
  `Costo_por_Km` double DEFAULT NULL,
  `Costo_por_Min` double DEFAULT NULL,
  `TarifaMinima` double DEFAULT NULL,
  `idCiudad` int NOT NULL,
  `idTipoRepartidor` int NOT NULL,
  PRIMARY KEY (`idTarifa`),
  KEY `fk_tarifa_ciudad1` (`idCiudad`),
  KEY `fk_tarifa_tiporepartidor1` (`idTipoRepartidor`),
  CONSTRAINT `fk_tarifa_ciudad1` FOREIGN KEY (`idCiudad`) REFERENCES `ciudad` (`idCiudad`),
  CONSTRAINT `fk_tarifa_tiporepartidor1` FOREIGN KEY (`idTipoRepartidor`) REFERENCES `tiporepartidor` (`idTipoRepartidor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tarifa`
--

LOCK TABLES `tarifa` WRITE;
/*!40000 ALTER TABLE `tarifa` DISABLE KEYS */;
/*!40000 ALTER TABLE `tarifa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tarjeta`
--

DROP TABLE IF EXISTS `tarjeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tarjeta` (
  `idtarjeta` int NOT NULL AUTO_INCREMENT,
  `last4` varchar(4) DEFAULT NULL,
  `brand` varchar(250) DEFAULT NULL,
  `status` tinyint DEFAULT NULL,
  `idUsuario` int NOT NULL,
  `idClienteConekta` varchar(250) DEFAULT NULL,
  `idTarjetaConekta` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`idtarjeta`),
  KEY `fk_tarjeta_persona1` (`idUsuario`),
  CONSTRAINT `fk_tarjeta_persona1` FOREIGN KEY (`idUsuario`) REFERENCES `persona` (`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tarjeta`
--

LOCK TABLES `tarjeta` WRITE;
/*!40000 ALTER TABLE `tarjeta` DISABLE KEYS */;
/*!40000 ALTER TABLE `tarjeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_documento`
--

DROP TABLE IF EXISTS `tipo_documento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipo_documento` (
  `idTipoDocumento` int NOT NULL,
  `Descripcion` varchar(200) NOT NULL,
  PRIMARY KEY (`idTipoDocumento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_documento`
--

LOCK TABLES `tipo_documento` WRITE;
/*!40000 ALTER TABLE `tipo_documento` DISABLE KEYS */;
/*!40000 ALTER TABLE `tipo_documento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoestablecimiento`
--

DROP TABLE IF EXISTS `tipoestablecimiento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipoestablecimiento` (
  `idTipoEstablecimiento` int NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(100) NOT NULL,
  `urlImagen` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`idTipoEstablecimiento`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipoestablecimiento`
--

LOCK TABLES `tipoestablecimiento` WRITE;
/*!40000 ALTER TABLE `tipoestablecimiento` DISABLE KEYS */;
INSERT INTO `tipoestablecimiento` VALUES (7,'Comida',NULL);
/*!40000 ALTER TABLE `tipoestablecimiento` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tiporepartidor`
--

DROP TABLE IF EXISTS `tiporepartidor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tiporepartidor` (
  `idTipoRepartidor` int NOT NULL AUTO_INCREMENT,
  `Descripcion` varchar(200) NOT NULL,
  PRIMARY KEY (`idTipoRepartidor`),
  UNIQUE KEY `idtable1_UNIQUE` (`idTipoRepartidor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tiporepartidor`
--

LOCK TABLES `tiporepartidor` WRITE;
/*!40000 ALTER TABLE `tiporepartidor` DISABLE KEYS */;
/*!40000 ALTER TABLE `tiporepartidor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipousuario`
--

DROP TABLE IF EXISTS `tipousuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tipousuario` (
  `idTipoUsuario` int NOT NULL,
  `Descripcion` varchar(45) NOT NULL,
  PRIMARY KEY (`idTipoUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipousuario`
--

LOCK TABLES `tipousuario` WRITE;
/*!40000 ALTER TABLE `tipousuario` DISABLE KEYS */;
INSERT INTO `tipousuario` VALUES (1,'Usuario'),(2,'Repartidor'),(3,'Admininistrador_Establecimiento'),(4,'Administrador_Ciudad'),(5,'Administrador_General');
/*!40000 ALTER TABLE `tipousuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tokenPush`
--

DROP TABLE IF EXISTS `tokenPush`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `tokenPush` (
  `idTokenPush` int NOT NULL AUTO_INCREMENT,
  `token` varchar(500) DEFAULT NULL,
  `plataforma` varchar(15) DEFAULT NULL,
  `idUsuario` int NOT NULL,
  PRIMARY KEY (`idTokenPush`),
  KEY `fk_tokenPush_persona1` (`idUsuario`),
  CONSTRAINT `fk_tokenPush_persona1` FOREIGN KEY (`idUsuario`) REFERENCES `persona` (`idUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tokenPush`
--

LOCK TABLES `tokenPush` WRITE;
/*!40000 ALTER TABLE `tokenPush` DISABLE KEYS */;
INSERT INTO `tokenPush` VALUES (3,'asdasdasasd56454asd4as56d4a6sd46as4d564as6d54a56sd46asd64asd548da9s8da9sd789f9qqw8w79qw8','web',2),(4,'asdasdasasd56454asd4as56d4a6sd46as4d564as6d54a56sd46asd64asd548da9s8da9sd789f9qqw8w79qw8','web',4);
/*!40000 ALTER TABLE `tokenPush` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaccion`
--

DROP TABLE IF EXISTS `transaccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaccion` (
  `idTransaccion` int NOT NULL AUTO_INCREMENT,
  `Monto` double NOT NULL,
  `Propina` double NOT NULL,
  `MontoTotal` double NOT NULL,
  `FechaAlta` datetime NOT NULL,
  `FechaPago` datetime NOT NULL,
  `idStatusTransaccion` int NOT NULL,
  `idViaje` int NOT NULL,
  `idMetodoPago` int NOT NULL,
  PRIMARY KEY (`idTransaccion`),
  KEY `fk_Transaccion_StatusTransaccion1` (`idStatusTransaccion`),
  KEY `fk_Transaccion_Viaje1` (`idViaje`),
  KEY `fk_Transaccion_MetodoPago1` (`idMetodoPago`),
  CONSTRAINT `fk_Transaccion_MetodoPago1` FOREIGN KEY (`idMetodoPago`) REFERENCES `metodopago` (`idMetodoPago`),
  CONSTRAINT `fk_Transaccion_StatusTransaccion1` FOREIGN KEY (`idStatusTransaccion`) REFERENCES `statustransaccion` (`idStatusTransaccion`),
  CONSTRAINT `fk_Transaccion_Viaje1` FOREIGN KEY (`idViaje`) REFERENCES `viaje` (`idViaje`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaccion`
--

LOCK TABLES `transaccion` WRITE;
/*!40000 ALTER TABLE `transaccion` DISABLE KEYS */;
/*!40000 ALTER TABLE `transaccion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `validaremail`
--

DROP TABLE IF EXISTS `validaremail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `validaremail` (
  `idValidarEmail` int NOT NULL AUTO_INCREMENT,
  `idPersona` int NOT NULL,
  `Status` int NOT NULL,
  `FechaSolicitd` datetime NOT NULL,
  `Email` varchar(70) NOT NULL,
  PRIMARY KEY (`idValidarEmail`),
  KEY `fk_ValidarEmail_Persona1` (`idPersona`),
  CONSTRAINT `fk_ValidarEmail_Persona1` FOREIGN KEY (`idPersona`) REFERENCES `persona` (`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `validaremail`
--

LOCK TABLES `validaremail` WRITE;
/*!40000 ALTER TABLE `validaremail` DISABLE KEYS */;
/*!40000 ALTER TABLE `validaremail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehiculo`
--

DROP TABLE IF EXISTS `vehiculo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vehiculo` (
  `idVehiculo` int NOT NULL AUTO_INCREMENT,
  `Placa` varchar(45) NOT NULL,
  `Modelo` varchar(45) NOT NULL,
  `Color` varchar(45) NOT NULL,
  `Año` varchar(45) NOT NULL,
  `Num_de_serie` varchar(100) NOT NULL,
  `Activo` int NOT NULL,
  `idUsuario` int NOT NULL,
  PRIMARY KEY (`idVehiculo`),
  KEY `fk_vehiculo_persona1` (`idUsuario`),
  CONSTRAINT `fk_vehiculo_persona1` FOREIGN KEY (`idUsuario`) REFERENCES `persona` (`idUsuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehiculo`
--

LOCK TABLES `vehiculo` WRITE;
/*!40000 ALTER TABLE `vehiculo` DISABLE KEYS */;
/*!40000 ALTER TABLE `vehiculo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `viaje`
--

DROP TABLE IF EXISTS `viaje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `viaje` (
  `idViaje` int NOT NULL AUTO_INCREMENT,
  `FechaSolicitud` datetime NOT NULL,
  `FechaInicio` datetime NOT NULL,
  `FechaFin` datetime NOT NULL,
  `CostoAproximado` double NOT NULL,
  `idRepartidor` int NOT NULL,
  `idStatusViaje` int NOT NULL,
  `idMetodoPago` int NOT NULL,
  `IdCarrito` int NOT NULL,
  `idDireccionOrigen` int NOT NULL,
  `idDireccionDestino` int NOT NULL,
  PRIMARY KEY (`idViaje`),
  KEY `fk_Viaje_Carrito1` (`IdCarrito`),
  KEY `fk_Viaje_Direccion1` (`idDireccionOrigen`),
  KEY `fk_Viaje_Direccion2` (`idDireccionDestino`),
  KEY `fk_Viaje_MetodoPago1` (`idMetodoPago`),
  KEY `fk_Viaje_Persona1` (`idRepartidor`),
  KEY `fk_Viaje_StatusViaje1` (`idStatusViaje`),
  CONSTRAINT `fk_Viaje_Carrito1` FOREIGN KEY (`IdCarrito`) REFERENCES `carrito` (`IdCarrito`),
  CONSTRAINT `fk_Viaje_Direccion1` FOREIGN KEY (`idDireccionOrigen`) REFERENCES `direccion` (`idDireccion`),
  CONSTRAINT `fk_Viaje_Direccion2` FOREIGN KEY (`idDireccionDestino`) REFERENCES `direccion` (`idDireccion`),
  CONSTRAINT `fk_Viaje_MetodoPago1` FOREIGN KEY (`idMetodoPago`) REFERENCES `metodopago` (`idMetodoPago`),
  CONSTRAINT `fk_Viaje_Persona1` FOREIGN KEY (`idRepartidor`) REFERENCES `persona` (`idUsuario`),
  CONSTRAINT `fk_Viaje_StatusViaje1` FOREIGN KEY (`idStatusViaje`) REFERENCES `statusviaje` (`idStatusViaje`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `viaje`
--

LOCK TABLES `viaje` WRITE;
/*!40000 ALTER TABLE `viaje` DISABLE KEYS */;
/*!40000 ALTER TABLE `viaje` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-08-07 17:38:14
