<?php
namespace App\Validation;
use App\Lib\Response;

class userValidation {
    public static function validate($data, $update = false) {
        $response = new Response();

        $key = 'nombre';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Nombre es obligatorio';
        } else {
            $value = $data[$key];

            if(strlen($value) < 3) {
                $response->errors[$key] = 'Nombre no vallido.';
            }
        }


        $key = 'apellidos';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Los apellidos son obligatorios';
        } else {
            $value = $data[$key];

            if(strlen($value) < 4) {
                $response->errors[$key] = 'Debe contener como mínimo 4 caracteres';
            }
        }

        $key = 'correo';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Este campo es obligatorio';
        } else {
            $value = $data[$key];

            if(!filter_var($value,FILTER_VALIDATE_EMAIL)) {
                $response->errors[$key] = 'Valor ingresado no es un correo';
            }
        }

        $key = 'password';
        if (!$update) {
          if(empty($data[$key])){
              $response->errors[$key]= 'Este campo es obligatorio';
          } else {
              $value = $data[$key];

              if(strlen($value) < 4) {
                  $response->errors[$key] = 'Debe contener como mínimo 4 caracteres';
              }
          }
        }else {
          if(!empty($data[$key])){
              $value = $data[$key];
              if(strlen($value) < 4) {
                  $response->errors[$key] = 'Debe contener como mínimo 4 caracteres';
              }
          }
        }
 
        $response->setResponse(count($response->errors) === 0);

        return $response;
    }
}
?>