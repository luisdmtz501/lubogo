<?php
namespace App\Validation;

use App\Lib\Response;

class studiesValidation {
    public static function validate($data, $update = false) {
        $response = new Response();
        //`Usuario_idUsuario`, `nivelEducativo`, `tituloEducativo`, `nombreEscuela`)

        $key = 'nivelEducativo';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Campo obligatorioo';
        } else {
            $value = $data[$key];

            if(strlen($value) <5) {
                $response->errors[$key] = 'Nivel educativo no valido.';
            }
        }


        $key = 'tituloEducativo';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Campo obligatorio';
        } else {
            $value = $data[$key];

            if(strlen($value) < 5) {
                $response->errors[$key] = 'Titulo educativo no valido.';
            }
        }

        $key = 'nombreEscuela';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Campo obligatorio';
        } else {
            $value = $data[$key];

            if(strlen($value) < 3) {
                $response->errors[$key] = 'Nombre no valido.';
            }
        }
 
        $response->setResponse(count($response->errors) === 0);

        return $response;
    }
}
?>