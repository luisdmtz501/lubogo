<?php
namespace App\Validation;

use App\Lib\Response;

class publicationValidation {
    public static function validate($data, $update = false) {
        $response = new Response();

        $key = 'Usuario_idUsuario';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Id de usuario obligatorio';
        } else {
            $value = $data[$key];            
        }


        $key = 'TipoPublicacion_idTipoPublicacion';
        if(empty($data[$key])) {
            $response->errors[$key] = 'El tipo de publicación es obligatorio.';
        } else {
            $value = $data[$key];

            if(strlen($value) > 4) {
                $response->errors[$key] = 'Tipo de publicacón no valida.';
            }
            if(strlen($value) < 0) {
                $response->errors[$key] = 'Tipo de publicacón no valida.';
            }
        }


        $key = 'tituloPublicacion';
        if(empty($data[$key])) {
            $response->errors[$key] = 'El titulo de publicación es obligatorio.';
        } else {
            $value = $data[$key];

            if(strlen($value) < 2) {
                $response->errors[$key] = 'Titulo no valido.';
            }
        }

        $key = 'descripcion';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Descripción de publicación es obligatorio.';
        } else {
            $value = $data[$key];

            if(strlen($value) < 5) {
                $response->errors[$key] = 'Descripción no valida.';
            }
        }

        $key = 'fechaHora';
        if(empty($data[$key])) {
            $response->errors[$key] = 'Fecha es obligatorio.';
        } else {
            $value = $data[$key];

            if(strlen($value) < 9) {
                $response->errors[$key] = 'Fecha no valida.';
            }
        }
 
        $response->setResponse(count($response->errors) === 0);

        return $response;
    }
}
?>