<?php
// Require the bundled autoload file - the path may need to change
// based on where you downloaded and unzipped the SDK
namespace App\Lib;

require __DIR__ . '/twilio-php-master/Twilio/autoload.php';

// Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client,
	App\Lib\Configuracion;


// Your Account SID and Auth Token from twilio.com/console
class SMS{
	public static function  SendMensaje($codigoPais,$telefono,$cuerpo)
	{
		$twilio = Configuracion::getTwilio();
		$sid = $twilio['sid'];
		$token = $twilio['token'];
		$from = $twilio['from'];

		$client = new Client($sid, $token);
		$client->messages->create(
				// the number you'd like to send the message to
				'+'.$codigoPais.$telefono.'',
				array(
				    // A Twilio phone number you purchased at twilio.com/console
					'from' => $from,#dev,
					'body' => $cuerpo
				)
			);
		return '1';
	}
}
?>