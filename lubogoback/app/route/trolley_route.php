<?php
use App\Lib\Response,
    App\Middleware\AuthMiddleware;

$app->group('/trolley/', function(){
	
	$this->post('addTrolley', function ($req, $res, $args) {
		return $res->withHeader('Content-type','application/json') 
				   ->write(
					 json_encode($this->model->trolley->addTrolley($req->getParsedBody()))
				  );
	 });
})->add(new AuthMiddleware($app));
  ?>