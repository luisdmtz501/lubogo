<?php 	
use App\Lib\Auth,
    App\Lib\Response,
    App\Middleware\AuthMiddleware;

$app->group('/user/', function () {
    $this->post('registerUser', function ($req, $res, $args) {
       return $res->withHeader('Content-type','application/json') 
                  ->write(
                    json_encode($this->model->user->registerUser($req->getParsedBody()))
                 );
    });

    $this->get('informationUser/{id}', function($req, $res, $args){
    	return $res->withHeader('Content-type', 'application/json')
    			   ->write(
    			   	 json_encode($this->model->user->informationUser($args['id']))
    			 );
    });

    $this->put('updateInformationUser/{idUsuario}', function($req, $res, $args){
    	return $res->withHeader('Content-type', 'application/json')
    			   ->write(
    			     json_encode($this->model->user->updateInformationUser($req->getParsedBody(), $args['idUsuario']))
    			 );
    });

    $this->delete('deleteUser/{id}', function($req, $res, $args){
    	return $res->withHeader('Content-type', 'application/json')
    			   ->write(
    			   	 json_encode($this->model->user->deleteUser($args['id']))
    			 );
    });

    $this->put('updateEmailUser/{id}', function($req, $res, $args){
        return $res->withHeader('Content-type', 'application/json')
                   ->write(
                     json_encode($this->model->user->updateEmailUser($req->getParsedBody(), $args['id']))
                 );
    });

    #addPhotoUser

    $this->get('toList/{idTipoUsuario}', function($req, $res, $args){
        return $res->withHeader('Content-type', 'application/json')
                   ->write(
                     json_encode($this->model->user->toList($args['idTipoUsuario']))
                 );
    });

    $this->put('updatePassword/{idUsuario}', function($req, $res, $args){
        $parametros = $req->getParsedBody();
        return $res->withHeader('Content-type', 'application/json')
                   ->write(
                     json_encode($this->model->user->updatePassword($args['idUsuario'], $parametros['Password']))
                 );
    });

    $this->put('login/{id}', function($req, $res, $args){
        return $res->withHeader('Content-type', 'application/json')
                   ->write(
                     json_encode($this->model->user->login($args['id']))
                 );
    });

    $this->get('obtainPhotoUser/{id}', function($req, $res, $args){
        return $res->withHeader('Content-type', 'application/json')
                   ->write(
                     json_encode($this->model->user->obtainPhotoUser($args['id']))
                   );
    });

})->add(new AuthMiddleware($app));