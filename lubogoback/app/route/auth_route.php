<?php
// use App\Lib\Auth,
    // App\Lib\Response,
    // App\Validation\userValidation
    // App\Middleware\AuthMiddleware;

$app->group('/auth/', function () {
    #validar numero
    $this->post('validateNumber', function ($req, $res, $args) {
      $parametros = $req->getParsedBody();
      return $res->withHeader('Content-type','application/json')
                   ->write(
                          json_encode($this->model->auth->validateNumber($parametros['codigoPais'],$parametros['telefono'],$parametros['idTipoUsuario']))
                   );
    });
    #validar codigo
    $this->post('validateCode', function ($req, $res, $args){
      $parametros = $req->getParsedBody();
      return $res->withHeader('Content-type','application/json')
              ->write(
                json_encode($this->model->auth->validateCode($parametros['codigoPais'],$parametros['telefono'],$parametros['codigo'])
              ));
    });
    
    #logout
    $this->post('logout', function ($req, $res, $args){
      $parametros = $req->getParsedBody();
      $id = $parametros["id"];
      return $res->withHeader('Content-type','application/json')
              ->write(
                json_encode($this->model->auth->logout($id)
              ));
    });

    #login User
    $this->post('loginUser', function ($req, $res, $args) {
      $parametros = $req->getParsedBody();
      $token = '';
      $plataforma = '';
      if (isset($parametros['token'])) {
        $token = $parametros['token'];
        $plataforma = $parametros['plataforma'];
      }
      if($token == null || $token == ''){
        $token = null;
      }
      if($plataforma == null || $plataforma == ''){
        $plataforma = null;
      }
        return $res->withHeader('Content-type','application/json')
                   ->write(
                      json_encode($this->model->auth->login($parametros['user'],$parametros['password'],1,$plataforma,$token))
                   );
    });
    #login Repartidor
    $this->post('loginDealer', function ($req, $res, $args) {
      $parametros = $req->getParsedBody();
      $token = '';
      $plataforma = '';
      if (isset($parametros['token'])) {
        $token = $parametros['token'];
        $plataforma = $parametros['plataforma'];
      }
      if($token == null || $token == ''){
        $token = null;
      }
      if($plataforma == null || $plataforma == ''){
        $plataforma = null;
      }
        return $res->withHeader('Content-type','application/json')
                   ->write(
                      json_encode($this->model->auth->login($parametros['user'],$parametros['password'],2,$plataforma,$token))
                   );
    });
    #login Administrador de Establecimeinto
    $this->post('loginAdminEstablishment', function ($req, $res, $args) {
      $parametros = $req->getParsedBody();
      $token = '';
      $plataforma = '';
      if (isset($parametros['token'])) {
        $token = $parametros['token'];
        $plataforma = $parametros['plataforma'];
      }
      if($token == null || $token == ''){
        $token = null;
      }
      if($plataforma == null || $plataforma == ''){
        $plataforma = null;
      }
        return $res->withHeader('Content-type','application/json')
                   ->write(
                      json_encode($this->model->auth->login($parametros['user'],$parametros['password'],3,$plataforma,$token))
                   );
    });
    #login Administrador Ciudad
    $this->post('loginAdminCity', function ($req, $res, $args) {
      $parametros = $req->getParsedBody();
      $token = '';
      $plataforma = '';
      if (isset($parametros['token'])) {
        $token = $parametros['token'];
        $plataforma = $parametros['plataforma'];
      }
      if($token == null || $token == ''){
        $token = null;
      }
      if($plataforma == null || $plataforma == ''){
        $plataforma = null;
      }
        return $res->withHeader('Content-type','application/json')
                   ->write(
                      json_encode($this->model->auth->login($parametros['user'],$parametros['password'],4,$plataforma,$token))
                   );
    });
    #login Administrador General
    $this->post('loginAdministrador', function ($req, $res, $args) {
      $parametros = $req->getParsedBody();
      $token = '';
      $plataforma = '';
      if (isset($parametros['token'])) {
        $token = $parametros['token'];
        $plataforma = $parametros['plataforma'];
      }
      if($token == null || $token == ''){
        $token = null;
      }
      if($plataforma == null || $plataforma == ''){
        $plataforma = null;
      }
        return $res->withHeader('Content-type','application/json')
                   ->write(
                      json_encode($this->model->auth->login($parametros['user'],$parametros['password'],5,$plataforma,$token))
                   );
    });
    #get data token
    $this->get('getData/{token}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                      json_encode($this->model->auth->getData($args['token']))
                   );
    });

    #Web - Recuperar password
    $this->post('sendCodeToEmail', function ($req, $res, $args) {
      $parametros = $req->getParsedBody();
        return $res->withHeader('Content-type','application/json')
          ->write(
            json_encode($this->model->auth->sendCodeToEmail($parametros['Email'], $parametros['TipoUsuario']))
          );
    });

    $this->post('validateCodeEmail/{idUsuario}', function ($req, $res, $args) {
      $parametros = $req->getParsedBody();
      return $res->withHeader('Content-type','application/json')
        ->write(
          json_encode($this->model->auth->validateCodeEmail($args['idUsuario'], $parametros['Codigo']))
        );
    });

});#->add(new Middleware($app)); #valida que en todas la validaciones en la cabecera se envie un token y va a validar dicho token