<?php 
use App\Lib\Auth,
    App\Lib\Response,
    App\Middleware\AuthMiddleware;

 $app->group('/city/', function(){
    $this->get('toList', function ($req, $res, $args){
		return $res->withHeader('Content-type', 'application/json')
				   ->write(
				   	 json_encode($this->model->city->toList())
				   );
	});
 })->add(new AuthMiddleware($app));
 ?>