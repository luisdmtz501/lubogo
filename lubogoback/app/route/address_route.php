<?php 
use App\Lib\Response,
	App\Middleware\AuthMiddleware;

$app->group('/address/', function(){
	$this->post('add', function ($req, $res, $args){
		return $res->withHeader('Content-type', 'application/json')
		   	 	   ->write(
		   	 	   	 json_encode($this->model->address->add($req->getParsedBody()))
		   	 	   );
	});

	$this->get('toList/{tipo}/{id}', function ($req, $res, $args){
		return $res->withHeader('Content-type', 'application/json')
				   ->write(
				   	 json_encode($this->model->address->toList($args['id'],$args['tipo']))
				   );
	});

	$this->get('obtain/{id}', function ($req, $res, $args){
		return $res->withHeader('Content-type', 'application/json')
				   ->write(
				   	 json_encode($this->model->address->obtain($args['id']))
				   );
	});

	$this->put('update/{id}', function($req, $res, $args){
		return $res->withHeader('Content-type', 'application/json')
				   ->write(
				   	 json_encode($this->model->address->update($req->getParsedBody(),$args['id']))
				   );
	});

	$this->delete('delete/{id}', function($req, $res, $args){
		return $res->withHeader('Content-type', 'application/json')
				   ->write(
				   	 json_encode($this->model->address->delate($args['id']))
				   );
	});

	$this->get('cp/{cp}',function($req,$res,$args){
        return $res->withHeader('Content-type','application/json')
                  ->write(
                       json_encode($this->model->address->cp($args['cp']))
                  );
    });

})->add(new AuthMiddleware($app));