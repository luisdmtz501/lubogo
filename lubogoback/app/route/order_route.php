<?php 
use App\Lib\Response,
	App\Middleware\AuthMiddleware;

$app->group('/order/', function(){
	$this->post('add', function ($req, $res, $args){
		return $res->withHeader('Content-type', 'application/json')
		   	 	   ->write(
		   	 	   	 json_encode($this->model->order->add($req->getParsedBody()))
		   	 	   );
	});

	$this->get('detail/{idOrder}',function($req, $res, $args){
		return $res->withHeader('Content-type', 'application/json')
				   ->write(
					json_encode($this->model->order->detail($args['idOrder']))
				   );
	});

	$this->get('list/{idUser}/{limit}/{offset}/{statusOrder}',function($req, $res, $args){
		return $res->withHeader('Content-type', 'application/json')
				   ->write(
					json_encode($this->model->order->list($args['idUser'],$args['limit'],$args['offset'],$args['statusOrder']))
				   );
	});

	$this->get('reporteOrdenes/{fechaInicio}/{fechaFin}/{statusOrder}',function($req, $res, $args){
		return $res->withHeader('Content-type', 'application/json')
				   ->write(
					json_encode($this->model->order->reporteOrdenes($args['fechaInicio'],$args['fechaFin'],$args['statusOrder']))
				   );
	});

	$this->put('finish',function($req, $res, $args){
		return $res->withHeader('Content-type', 'application/json')
				   ->write(
					json_encode($this->model->order->finish($req->getParsedBody()))
			   );
	});
	
	$this->get('detailDealer/{idOrder}',function($req, $res, $args){
		return $res->withHeader('Content-type', 'application/json')
				   ->write(
					json_encode($this->model->order->detailDealer($args['idOrder']))
				   );
	});

	$this->put('updateStatus/{idOrder}/{status}',function($req, $res, $args){
		return $res->withHeader('Content-type', 'application/json')
				   ->write(
					json_encode($this->model->order->updateStatus($args['idOrder'],$args['status'], $req->getParsedBody()))
				   );
	});
	
	$this->get('push/{id}',function ($req,$res,$args){
		return $res->withHeader('Content-type', 'application/json')
				   ->write(
					json_encode($this->model->order->push($args['id']))
				   );
	});

	$this->get('reportDashboard/{de}/{a}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->order->reportDashboard($args['de'],$args['a']))
                   );
	});
	
	$this->get('reportDashboardUser/{de}/{a}', function ($req, $res, $args) {
        return $res->withHeader('Content-type','application/json')
                   ->write(
                    json_encode($this->model->order->reportDashboardUser($args['de'],$args['a']))
                   );
    });

})->add(new AuthMiddleware($app));