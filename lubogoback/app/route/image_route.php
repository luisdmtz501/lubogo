<?php 	
use App\Lib\Auth,
    App\Lib\Response,
    App\Middleware\AuthMiddleware;

$app->group('/image/', function () {
  $this->post('addPhotoUser/{idUsuario}', function ($req, $res, $args) {
    $file = $_FILES;
    return $res->withHeader('Content-type','application/json')
               ->write(
                 json_encode($this->model->image->addPhotoUser($file, $args['idUsuario']))
               );
  });

  $this->post('add/{tipo}/{id}', function ($req, $res, $args) {
    $file = $_FILES;
    return $res->withHeader('Content-type','application/json')
               ->write(
                 json_encode($this->model->image->add($file, $args['tipo'], $args['id']))
               );
  });

  $this->post('addEstablishmentMenuImage/{idEstablecimiento}', function ($req, $res, $args) {
    $file = $_FILES;
    return $res->withHeader('Content-type','application/json')
      ->write(
        json_encode($this->model->image->addEstablishmentMenuImage($file, $args['idEstablecimiento']))
      );
  });

  $this->delete('deleteImage/{idGaleria}', function ($req, $res, $args) {
    return $res->withHeader('Content-type', 'application/json')
               ->write(
                  json_encode($this->model->image->deleteImage($args['idGaleria']))
                );            
  });
  
  $this->get('listImage/{idEstablecimiento}', function ($req, $res, $args) {
		return $res->withHeader('Content-type', 'application/json')
				       ->write(
					      json_encode($this->model->image->listImage($args['idEstablecimiento']))
				       );
  });

  $this->post('updateUri/{tipo}', function ($req, $res, $args){
    return $res->withHeader('Content-type', 'application/json')
               ->write(
                  json_encode($this->model->image->updateUri($args['tipo']))
               );
  });

});