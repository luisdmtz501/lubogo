<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\RestApi,
    App\Lib\RestApiMethod,
    App\Lib\Push;

class promotion_announcementModel{

    private $db;
    private $tbPromociones_Anuncios = 'promociones_anuncios';
    private $tbtipoPromociones_Anuncios = 'tipoPromociones_Anuncios';
    private $tbEstablecimiento = 'establecimiento';
    private $tbBoletos = 'boleto';
    private $response;

    public function __CONSTRUCT($db) {
        $this->db = $db;
        $this->response = new Response();
    }

    //Agregar
    public function add($data){
	$addPromocionAnuncio = $this->db->insertInto($this->tbPromociones_Anuncios, $data)
					->execute();
		
	    if ($addPromocionAnuncio != false) {
				   $this->response->result = $addPromocionAnuncio;
		    return $this->response->SetResponse(true,'Agregado con exito');
		}else{
                    $this->response->errors='Error al agregar los datos';
            return  $this->response->SetResponse(false);
        }
    }
    // update
    public function update($data,$id){
        $addPromocionAnuncio = $this->db->update($this->tbPromociones_Anuncios, $data)
                                        ->where('idPromociones_Anuncios',$id)
                                        ->execute();
		
	    if ($addPromocionAnuncio != false) {
				   $this->response->result = $addPromocionAnuncio;
		    return $this->response->SetResponse(true,'Actualizdo con exito');
		}else{
                    $this->response->errors='Error al agregar los datos';
            return  $this->response->SetResponse(false);
        }
    }
    
    //Listar
	public function listTipo(){
        $listar = $this->db->from($this->tbtipoPromociones_Anuncios)
						//  ->where('idTipoPromociones_Anuncios 3 ')
						 ->orderBy('idTipoPromociones_Anuncios DESC')
						 ->fetchAll();

		if	($listar !=false)	{
				   $this->response->result=['Lista' => $listar];
			return $this->response->SetResponse(true);
		}else{
				   $this->response->errors='No existe promociones y/o anuncios';
			return $this->response->SetResponse(false);
        }
    }

    // promociones_anuncios.idPromociones_Anuncios, promociones_anuncios.titulo AS Detalle, 
    //                      establecimiento.idEstablecimiento,
    //                      IFNULL(establecimiento.nombre, "Sin detalle") AS Establecimiento,
    //                      IFNULL(promociones_anuncios.urlImagen, "Sin detalle") AS UrlImag, 
    //                      IFNULL(tipoPromociones_Anuncios.descripcion, "Sin detalle") AS Promociones_o_Anuncios,
    //                      IFNULL(promociones_anuncios.fechaExpiracion, "Sin detalle") AS FechaExpiración,
    //                      IFNULL(promociones_anuncios.codigoDescuento, "Sin detalle") AS CodigoDescuento,
    //                      IFNULL(promociones_anuncios.montoDescuento, "Sin detalle") AS MontoDescuento

    public function list(){
        $listar = $this->db->from($this->tbPromociones_Anuncios)
                         ->select(null)
                         ->select('promociones_anuncios.idPromociones_Anuncios, promociones_anuncios.descripcion AS detalle,
                         IFNULL(establecimiento.idEstablecimiento, "Sin detalle") AS idEstablecimiento,
                         IFNULL(promociones_anuncios.urlImagen, "Sin detalle") AS UrlImag,
                         IFNULL(tipoPromociones_Anuncios.idTipoPromociones_Anuncios, "Sin detalle") AS idTipoPromociones_Anuncios,
                         IFNULL(promociones_anuncios.fechaExpiracion, "Sin detalle") AS FechaExpiración,
                         IFNULL(promociones_anuncios.codigoDescuento, "Sin detalle") AS CodigoDescuento,
                         IFNULL(promociones_anuncios.montoDescuento, "Sin detalle") AS MontoDescuento,
                         IFNULL(promociones_anuncios.titulo, "Sin detalle") AS titulo,
                         IFNULL(promociones_anuncios.nota, "Sin detalle") AS nota')
                         ->leftJoin('establecimiento on establecimiento.idEstablecimiento = promociones_anuncios.idEstablecimiento')
                         ->leftJoin('tipoPromociones_Anuncios on tipoPromociones_Anuncios.idTipoPromociones_Anuncios = promociones_anuncios.idTipoPromociones_Anuncios')
						 ->where('idPromociones_Anuncios')
						 ->orderBy('idPromociones_Anuncios DESC')
						 ->fetchAll();

		if	($listar !=false)	{
				   $this->response->result=['Lista' => $listar];
			return $this->response->SetResponse(true);
		}else{
				   $this->response->errors='No existe promociones y/o anuncios';
			return $this->response->SetResponse(false);
		}
    }
    //Detalle de promociones y/o Anuncios
    public function detail($id){
        $detail = $this->db->from($this->tbPromociones_Anuncios)
            ->select(null)
            ->select('
                promociones_anuncios.idPromociones_Anuncios, promociones_anuncios.descripcion AS detalle,
                IFNULL(establecimiento.idEstablecimiento, "Sin detalle") AS idEstablecimiento,
                IFNULL(promociones_anuncios.urlImagen, "Sin detalle") AS UrlImag,
                IFNULL(tipoPromociones_Anuncios.idTipoPromociones_Anuncios, "Sin detalle") AS idTipoPromociones_Anuncios,
                IFNULL(promociones_anuncios.fechaExpiracion, "Sin detalle") AS FechaExpiración,
                IFNULL(promociones_anuncios.codigoDescuento, "Sin detalle") AS CodigoDescuento,
                IFNULL(promociones_anuncios.montoDescuento, "Sin detalle") AS MontoDescuento,
                IFNULL(promociones_anuncios.titulo, "Sin detalle") AS titulo,
                IFNULL(promociones_anuncios.nota, "Sin detalle") AS nota')
            ->leftJoin('establecimiento on establecimiento.idEstablecimiento = promociones_anuncios.idEstablecimiento')
            ->leftJoin('tipoPromociones_Anuncios on tipoPromociones_Anuncios.idTipoPromociones_Anuncios = promociones_anuncios.idTipoPromociones_Anuncios')
            ->where('promociones_anuncios.idPromociones_Anuncios', $id)
            ->fetch();

        if	($detail != false) {
                   $this->response->result=$detail;
            return $this->response->SetResponse(true);
        }else{
                   $this->response->errors='Error, este establecimiento no cuenta con promociones y/o anuncios';
            return $this->response->SetResponse(false);
        }
    }
    //Eliminar
    public function delete($idPromociones_Anuncios){
		$eliminar = $this->db->deleteFrom($this->tbPromociones_Anuncios)
				 ->where('idPromociones_Anuncios',$idPromociones_Anuncios)
				 ->execute();

		if	($eliminar != false) {
				   $this->response->result=$eliminar;
			return $this->response->SetResponse(true, 'Se ha eliminado exitosamente');
		}else{
				   $this->response->errors='Error, al eliminar la promoción y/o anuncio, verifique nuevamente!!';
			return $this->response->SetResponse(false);
        }
    }

    public function listarBoletos($idUser){
        $data = $this->db->from($this->tbBoletos)
                         ->select(null)
                         ->select('idboleto')
                         ->where('idUsuario',$idUser)
                         ->fetchAll();
        if($data){
            foreach($data as $valor){
                $ordenes = $this->db->from("detalleBoleto")
                            ->select(null)
                            ->select('idOrden')
                            ->where('idboleto',$valor->idboleto)
                            ->fetchAll();
                $cadena = 'Boleto correspondiente a los pedidos: ';
                foreach($ordenes as $element){
                    $cadena .= "$element->idOrden,";
                }
                $cadena = substr($cadena, 0, -1);
                $valor->boletos = $cadena;

            }
            $this->response->result=$data;
			return $this->response->SetResponse(true);
        }else{
            $this->response->errors='No hay boletos';
			return $this->response->SetResponse(false,'No cuenta con boletos');
        }
    }

}