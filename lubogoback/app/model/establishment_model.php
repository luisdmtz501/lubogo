<?php 
namespace App\Model;

use App\Lib\Response,
	App\Lib\Cifrado,
	App\Lib\Configuracion,
	App\Lib\DayHour;
	
use Conekta\Order;

class EstablishmentModel
{
	private $db;
	private $response;
	private $tableEstablishmnet = 'establecimiento';
	private $tableHasSubTypeEstablishmnet = 'establecimiento_has_subcategorias';
	private $tableTypeEstablishment = 'tipoestablecimiento';
	private $tableSubTypeEstablishmnet = 'subcategorias';
	private $tableSchedule = 'horario';
	
	function __CONSTRUCT($db)
	{
		$this->db = $db;
        $this->response = new Response();
	}

	#Servicios	
	public function addEstablishment($data){
			$subcategorias = false; 
			// existe subcategorias
			if (isset($data['subcategorias'])) {
				$subcategorias = $data['subcategorias'];
				unset($data['subcategorias']);
			}
			// idAdmin 0
			if ($data['idAdministrador'] == 0) {
				unset($data['idAdministrador']);
			}
			// alta de establecimiento
			$register = $this->db->insertInto($this->tableEstablishmnet, $data)
								 ->execute();
								 
			if ($register > 0 ) {
				// alta de subcategorias
				if ($subcategorias != false) {
					$arraySubcategorias = explode("-",$subcategorias);
					foreach ($arraySubcategorias as $value) {
						$dataSub = array(
							"idEstablecimiento"=>$register,
							"idSubCategorias"=>$value
						);
						$insertarSubcategorias = $this->db->insertInto($this->tableHasSubTypeEstablishmnet,$dataSub)
													      ->execute();
					}
				}
				
					   $this->response->result = $register;
				return $this->response->SetResponse(true, "Registro exitoso");
			}
	}

	public function obtain($id){
		$obtener = $this->db->from($this->tableEstablishmnet)
							->select('direccion.idDireccion,direccion.calle,direccion.numeroExterior,direccion.numeroInterior,direccion.Municipio,direccion.Colonia,direccion.Ciudad,direccion.Latitud,direccion.Longitud,direccion.Estado,direccion.CodPostal')
							->where('establecimiento.idEstablecimiento', $id)
							->leftJoin('direccion on direccion.idEstablecimiento = establecimiento.idEstablecimiento AND direccion.Guardado = 1')
							->fetch();
		
		$subcategorias = $this->db->from($this->tableEstablishmnet)
								  ->select(null)
								  ->select('subcategorias.descripcion,subcategorias.idSubCategorias')
								  ->leftJoin('establecimiento_has_subcategorias AS bridge  on  establecimiento.idEstablecimiento = bridge.idEstablecimiento')
								  ->leftJoin('subcategorias on subcategorias.idSubCategorias = bridge.idSubCategorias')
								  ->where('establecimiento.idEstablecimiento',$id)
								  ->fetchAll();
		$obtener = get_object_vars($obtener); //convertir objeto en arreglo
		//$subcategorias = get_object_vars($subcategorias); //convertir objeto en arreglo
		// var_dump($obtener);
		// var_dump($subcategorias);
		$sub = array(
			'subcategorias'=>$subcategorias
		);
		$envio = array(
			'envio'=>"25"
		);
		$new_data = array_merge($obtener, $sub);
		$new_data = array_merge($new_data,$envio);
				   $this->response->result = $new_data;		
			return $this->response->SetResponse(true);
	}
	// TODO tiempo estimado, costo de envio, numero de pedidos que ha realizado el establecimiento,
	 public function toListForSubcategories($idSubcategoria){
		$data = $this->db->from($this->tableEstablishmnet)
						 ->select(null)
						 ->select('establecimiento.idEstablecimiento, nombre AS NombreEstablecimiento,urlFoto AS UrlImag, telefono AS Telefono,
						 CONCAT(direccion.Calle, " ", 
						 "#", direccion.numeroExterior, " ", 
					 	 direccion.colonia) AS Direccion,
						 calificacion AS Calificacion, IFNULL(direccion.Latitud,0) AS Latitud,IFNULL(direccion.Longitud, 0) AS Longitud,tipoestablecimiento.Descripcion AS Categoria, ciudad.CostoEnvio,   "Abierto" AS StatusHorario, establecimiento.urlPortada as UrlPortada')
						 ->leftJoin('direccion on direccion.idEstablecimiento = establecimiento.idEstablecimiento')
						 ->leftJoin('tipoestablecimiento on tipoestablecimiento.idTipoEstablecimiento = establecimiento.idTipoEstablecimiento')
						 ->leftJoin('establecimiento_has_subcategorias as bridge on establecimiento.idEstablecimiento = bridge.idEstablecimiento')
						 ->leftJoin('subcategorias on subcategorias.idSubCategorias = bridge.idSubCategorias')
						 ->leftJoin('ciudad ON ciudad.idCiudad = establecimiento.idCiudad')
						 ->where('subcategorias.idSubCategorias',$idSubcategoria)
						 ->where('idStatusEstablecimiento', 1)
						 ->groupBy('establecimiento.idEstablecimiento')
						 ->orderBy('RAND()')
    					 ->fetchAll();

		$data = $this->agregarSubcaterias($data);
		$data = $this->StatusHorario($data);
		
    		   $this->response->result = $data;
    	return $this->response->SetResponse(true);
	}

	public function toListForCategories($idCategoria){
		$data = $this->db->from($this->tableEstablishmnet)
						 ->select(null)
						 ->select('establecimiento.idEstablecimiento, nombre AS NombreEstablecimiento,urlFoto AS UrlImag, telefono AS Telefono,
						 CONCAT(direccion.Calle, " ", 
						 "#", direccion.numeroExterior, " ", 
					 	 direccion.colonia) AS Direccion,
						 calificacion AS Calificacion, IFNULL(direccion.Latitud,0) AS Latitud,IFNULL(direccion.Longitud, 0) AS Longitud,tipoestablecimiento.Descripcion AS Categoria, ciudad.CostoEnvio,   "Abierto" AS StatusHorario, establecimiento.urlPortada as UrlPortada')
						 ->leftJoin('direccion on direccion.idEstablecimiento = establecimiento.idEstablecimiento')
						 ->leftJoin('tipoestablecimiento on tipoestablecimiento.idTipoEstablecimiento = establecimiento.idTipoEstablecimiento')
						 ->leftJoin('establecimiento_has_subcategorias as bridge on establecimiento.idEstablecimiento = bridge.idEstablecimiento')
						 ->leftJoin('subcategorias on subcategorias.idSubCategorias = bridge.idSubCategorias')
						 ->leftJoin('ciudad ON ciudad.idCiudad = establecimiento.idCiudad')
						 ->where('establecimiento.idTIpoEstablecimiento',$idCategoria)
						 ->where('idStatusEstablecimiento', 1)
						 ->groupBy('establecimiento.idEstablecimiento')
						 ->orderBy('RAND()')
						 ->fetchAll();

		$data = $this->agregarSubcaterias($data);
		$data = $this->StatusHorario($data);

			   $this->response->result = $data;
		return $this->response->SetResponse(true);
	}

	public function searchLike($like){
		$like = "%$like%";
						 
		$data = $this->db->from($this->tableEstablishmnet)
						 ->select(null)
						 ->select('establecimiento.idEstablecimiento, nombre AS NombreEstablecimiento,urlFoto AS UrlImag, telefono AS Telefono, 
						 CONCAT(direccion.Calle, " ", 
						 "#", direccion.numeroExterior, " ", 
					 	 direccion.colonia) AS Direccion,
						 calificacion AS Calificacion, IFNULL(direccion.Latitud,0) AS Latitud,IFNULL(direccion.Longitud, 0) AS Longitud,tipoestablecimiento.Descripcion AS Categoria, ciudad.CostoEnvio,   "Abierto" AS StatusHorario, establecimiento.urlPortada as UrlPortada')
						 ->leftJoin('direccion on direccion.idEstablecimiento = establecimiento.idEstablecimiento')
						 ->leftJoin('ciudad ON ciudad.idCiudad = establecimiento.idCiudad')
						 ->leftJoin('tipoestablecimiento on tipoestablecimiento.idTipoEstablecimiento = establecimiento.idTipoEstablecimiento')
						 ->where("etiquetas like :lik AND idStatusEstablecimiento = 1",[":lik"=>$like])
						 ->groupBy('establecimiento.idEstablecimiento')
						 ->orderBy('RAND()')
						 ->fetchAll();

		$data = $this->agregarSubcaterias($data);
		$data = $this->StatusHorario($data);

			   $this->response->result = $data;
		return $this->response->SetResponse(true);
	}

	public function update($id,$data){
		$obtener = $this->db->from($this->tableEstablishmnet)
						   ->where('idEstablecimiento', $id)
						   ->fetch();
		if ($obtener != false) {
			$subcategorias = false;
			//existe subcategorias
			if (isset($data['subcategorias'])) {
				$subcategorias = $data['subcategorias'];
				unset($data['subcategorias']);
				// se eliminan las anteriores 
				$eliminarSubcategorias = $this->db->deleteFrom($this->tableHasSubTypeEstablishmnet)
												  ->where('idEstablecimiento',$id)
												   ->execute();
				// alta nuevas subcategorias
				$arraySubcategorias = explode("-",$subcategorias);
				foreach ($arraySubcategorias as $value) {
					$dataSub = array(
						"idEstablecimiento"=>$id,
						"idSubCategorias"=>$value
					);
					$insertarSubcategorias = $this->db->insertInto($this->tableHasSubTypeEstablishmnet,$dataSub)
													  ->execute();
				}
			}
			if (count($data) > 0) {
						if ($data['idAdministrador'] == 0) {
							$data['idAdministrador'] = NULL;
						}
					   $actualizar = $this->db->update($this->tableEstablishmnet, $data) 
											  ->where('idEstablecimiento',$id)          
											  ->execute();

					   $this->response->result = $data;
				return $this->response->SetResponse(true);
			}else {
				if ($subcategorias != false) {
							$this->response->result = $subcategorias;
					return 	$this->response->SetResponse(true);
				}else{
							$this->response->errors = "no se actualizo ningun dato";
					return 	$this->response->SetResponse(false);
				}
			}
			
		}else{
				   $this->response->errors = "No existe este Establecimiento";
			return $this->response->SetResponse(true);
		}
	}

    public function delete($id){
    	$this->db->update($this->tableEstablishmnet)
				 ->set('idStatusEstablecimiento', 2) #set actualiza la columna indicada por el valor indicado
				 ->where('idEstablecimiento', $id)
				 ->execute();

		return $this->response->SetResponse(true, 'Se ha eliminado exitosamente');
    }

    #listar categorias y sub categorias
    public function listCategories(){
    	$data = $this->db->from($this->tableTypeEstablishment)
    					 ->select(null)
						 ->select('tipoestablecimiento.idTipoEstablecimiento, descripcion, urlImagen, fondoCategoria.Url As Fondo')
						 ->leftJoin('fondoCategoria on fondoCategoria.idTipoEstablecimiento = tipoestablecimiento.idTipoEstablecimiento')
						 ->where('statusCategoria = 1')
						 ->orderBy('idTipoEstablecimiento DESC') #ASC
						 ->fetchAll();
						 
    		   $this->response->result = $data;
    	return $this->response->SetResponse(true);
    }
	public function listCategoriesApp(){
    	$data = $this->db->from($this->tableTypeEstablishment)
    					 ->select(null)
						 ->select('tipoestablecimiento.idTipoEstablecimiento, descripcion, urlImagen,fondoCategoria.Url As Fondo')
						 ->leftJoin('fondoCategoria on fondoCategoria.idTipoEstablecimiento = tipoestablecimiento.idTipoEstablecimiento')
						 ->where('statusCategoria = 1 AND tipoestablecimiento.idTipoEstablecimiento != 7')
						 ->orderBy('tipoestablecimiento.descripcion ASC') #ASC
    					 ->fetchAll();
		// obtener num de estabs
		foreach($data as $element){
			$needList  = $this->db->from($this->tableEstablishmnet)
								  ->select(null)
								  ->select("count(*) As NumEstab")
								  ->where("idTipoEstablecimiento",$element->idTipoEstablecimiento)
								  ->fetch()
								  ->NumEstab;
			if($needList == 1){
				$estabDefault = $this->db->from($this->tableEstablishmnet)
							 ->select(null)
							 ->select('establecimiento.idEstablecimiento, nombre AS NombreEstablecimiento,urlFoto AS UrlImag, telefono AS Telefono,calificacion AS Calificacion,IFNULL(direccion.Latitud,0) AS Latitud,IFNULL(direccion.Longitud, 0) AS Longitud,tipoestablecimiento.Descripcion AS Categoria,ciudad.CostoEnvio,"Abierto" AS StatusHorario, establecimiento.urlPortada as UrlPortada')
							 ->leftJoin('direccion on direccion.idEstablecimiento = establecimiento.idEstablecimiento')
							 ->leftJoin('tipoestablecimiento on tipoestablecimiento.idTipoEstablecimiento = establecimiento.idTipoEstablecimiento')
							 ->leftJoin('ciudad ON ciudad.idCiudad = establecimiento.idCiudad')
							 ->where('idStatusEstablecimiento != 2')
							 ->where('establecimiento.idTipoEstablecimiento',$element->idTipoEstablecimiento)
							 ->groupBy('establecimiento.idEstablecimiento')
							 ->orderBy('RAND()')
							 ->fetchAll();
				$estabDefault = $this->agregarSubcaterias($estabDefault);
				$estabDefault = $this->StatusHorario($estabDefault);

				$element->NeedList = false;
				$element->EstabDefault = $estabDefault[0];
			}else{
				$element->NeedList = true;
				$element->EstabDefault = null;
			}
		}

    		   $this->response->result = $data;
    	return $this->response->SetResponse(true);
    }
	public function listSubcategories($id){
		$data = $this->db->from($this->tableSubTypeEstablishmnet)
						 ->where('idTipoEstablecimiento',$id)
						 ->where('statusSubcategoria = 1')
    					 ->fetchAll();

    		   $this->response->result = $data;
    	return $this->response->SetResponse(true);
	}

	public function addCategoria($data){
		// alta de establecimiento
		$register = $this->db->insertInto($this->tableTypeEstablishment, $data)
							 ->execute();
			   $this->response->result= $register;
		return $this->response->SetResponse(true);
	}
	
	public function addSubcategoria($data){
		// alta de subcategoria
		$register = $this->db->insertInto($this->tableSubTypeEstablishmnet, $data)
							 ->execute();
			   $this->response->result= $register;
		return $this->response->SetResponse(true);
	}

	public function updateCategoria($data,$id){
		// actualizar categoria
		$register = $this->db->update($this->tableTypeEstablishment, $data)
							 ->where('idTIpoEstablecimiento',$id)
							 ->execute();
			   $this->response->result= $register;
		return $this->response->SetResponse(true);
	}
	
	public function updateSubcategoria($data,$id){
		// actualizar subcategoria
		$register = $this->db->update($this->tableSubTypeEstablishmnet, $data)
							 ->where('idSubCategorias',$id)
							 ->execute();
			   $this->response->result= $register;
		return $this->response->SetResponse(true);
	}

	public function deleteCategoria($id){
		$register = $this->db->update($this->tableTypeEstablishment)
							 ->set('statusCategoria',0)
							 ->where('idTIpoEstablecimiento',$id)
							 ->execute();
			   $this->response->result= $register;
		return $this->response->SetResponse(true);
	}

	public function deleteSubcategoria($id){
		$register = $this->db->update($this->tableSubTypeEstablishmnet)
							 ->set('statusSubcategoria',0)
							 ->where('idSubCategorias',$id)
							 ->execute();
			   $this->response->result= $register;
		return $this->response->SetResponse(true);
	}
	#
	public function list(){
		$data = $this->db->from($this->tableEstablishmnet)
						 ->select(null)
						 ->select('establecimiento.idEstablecimiento, nombre AS NombreEstablecimiento,urlFoto AS UrlImag, telefono AS Telefono,
						 CONCAT(direccion.Calle, " ", 
						 "#", direccion.numeroExterior, " ", 
						 direccion.colonia) AS Direccion,
						 calificacion AS Calificacion,IFNULL(direccion.Latitud,0) AS Latitud,IFNULL(direccion. Longitud, 0) AS Longitud,tipoestablecimiento.Descripcion AS Categoria,ciudad.CostoEnvio,"Abierto" AS StatusHorario, establecimiento.urlPortada as UrlPortada')
						 ->leftJoin('direccion on direccion.idEstablecimiento = establecimiento.idEstablecimiento')
						 ->leftJoin('tipoestablecimiento on tipoestablecimiento.idTipoEstablecimiento = establecimiento.idTipoEstablecimiento')
						 ->leftJoin('ciudad ON ciudad.idCiudad = establecimiento.idCiudad')
						 ->where('idStatusEstablecimiento != 2 AND establecimiento.idTipoEstablecimiento = 7')
						 ->groupBy('establecimiento.idEstablecimiento')
						 ->orderBy('RAND()')
						 ->fetchAll();

		$data = $this->agregarSubcaterias($data);
		$data = $this->StatusHorario($data);

				$this->response->result= $data;
		 return $this->response->SetResponse(true);
	}

	public function listAdmin(){
		$data = $this->db->from($this->tableEstablishmnet)
						 ->select(null)
						 ->select('establecimiento.idEstablecimiento, nombre AS NombreEstablecimiento,urlFoto AS UrlImag, telefono AS Telefono,calificacion AS Calificacion,IFNULL(direccion.Latitud,0) AS Latitud,IFNULL(direccion.Longitud, 0) AS Longitud,tipoestablecimiento.Descripcion AS Categoria,ciudad.CostoEnvio,"Abierto" AS StatusHorario, establecimiento.urlPortada as UrlPortada')
						 ->leftJoin('direccion on direccion.idEstablecimiento = establecimiento.idEstablecimiento')
						 ->leftJoin('tipoestablecimiento on tipoestablecimiento.idTipoEstablecimiento = establecimiento.idTipoEstablecimiento')
						 ->leftJoin('ciudad ON ciudad.idCiudad = establecimiento.idCiudad')
						 ->where('idStatusEstablecimiento != 2')
						 ->groupBy('establecimiento.idEstablecimiento')
						 ->orderBy('idEstablecimiento DESC')
						 ->fetchAll();

		$data = $this->agregarSubcaterias($data);

				$this->response->result= $data;
		 return $this->response->SetResponse(true);
	}

	public function agregarSubcaterias($data){
		foreach ($data as $element) {
			// $element["idEstablecimiento"];
			$subcategorias = $this->db->from($this->tableHasSubTypeEstablishmnet)
									->select()
									->select('descripcion')
									->leftJoin('subcategorias on subcategorias.idSubCategorias = establecimiento_has_subcategorias.idSubCategorias')
									->where('idEstablecimiento',$element->idEstablecimiento)
									->fetchAll();
			$cadena = '';
			foreach ($subcategorias as $value) {
				$cadena.="$value->descripcion,";
			}
			$cadena = substr($cadena, 0, -1);
			$element->Categoria = $cadena;
		}
		return $data;
	}

	public function StatusHorario($data){
		$diaHora = DayHour::get();
		$hora = $diaHora["hora"];
		$dia = $diaHora["dia"];
		foreach ($data as $element) {
			$id = $element->idEstablecimiento;
			// SELECT true as StatusHorario FROM lubogo.horario where NOW() BETWEEN horaApertura and horaCierre and status = 1 and iddias = 7 and idEstablecimiento = 4
			$horario = $this->db->from('horario')
								->select(null)
								->select(' 1 as StatusHorario, TIME_FORMAT(horaApertura,"%h-%i %p") AS HoraA,TIME_FORMAT(horaCierre,"%h-%i %p") AS HoraC')
								->where("$hora BETWEEN horaApertura and horaCierre and status = 1 and iddias = $dia and idEstablecimiento = $id")
								->fetch();
			$cadena = "";
			$horarioHoy = "";
			if($horario->StatusHorario == 1){
				$cadena = "Abierto";
				$horaA = $horario->HoraA;
				$horaA = str_replace("-", ":", $horaA);
				$horaC = $horario->HoraC;
				$horaC = str_replace("-", ":", $horaC);
				$horarioHoy = "Horario hoy: $horaA a $horaC";
			}else{
				$cadena = "Cerrado";
				$horarioHoy = "Cerrado";
			}
			$element->StatusHorario = $cadena;
			$element->HorarioHoy = $horarioHoy;
		}
		usort($data, $this->object_sorter('StatusHorario'));
		return $data;
	}

	public function object_sorter($clave,$orden=null) {
		return function ($a, $b) use ($clave,$orden) {
			  $result=  ($orden=="DESC") ? strnatcmp($b->$clave, $a->$clave) :  strnatcmp($a->$clave, $b->$clave);
			  return $result;
		};
	}
	

}