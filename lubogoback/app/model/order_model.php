<?php
namespace App\Model;

use App\Lib\Response,
    App\Lib\RestApi,
    App\Lib\RestApiMethod,
    App\Lib\Push,
    App\Lib\DayHour;

class OrderModel{
    private $db;
    private $tbEstablecimiento = 'establecimiento';
    private $tbPersona = 'persona';
    private $tbCoordenada = 'coordenada';
    private $tbDireccion = 'direccion';
    private $tbOrden = 'orden';
    private $tbCancelarPedido = 'cancelarpedido';
    private $tbcountOrdenes = 'countOrdenes';
    private $tbTrolley = 'carrito';
    private $response;

    public function __CONSTRUCT($db) {
        $this->db = $db;
        $this->response = new Response();
    }

    public function add($data) {
        $id_establecimiento = null;
        $id_ciudad = 2;
        $id_destino = $data['idDireccionDestino'];
        $costo_envio = 25;

        if ($data['idEstablecimiento'] != null) {
            //VALIDAR APERTURA DE NEGOCIO
            $diaHora = DayHour::get();
            $hora = $diaHora["hora"];
            $dia = $diaHora["dia"];
            $id = $data['idEstablecimiento'];
			// SELECT true as StatusHorario FROM lubogo.horario where NOW() BETWEEN horaApertura and horaCierre and status = 1 and iddias = 7 and idEstablecimiento = 4
			$horario = $this->db->from('horario')
				->select(null)
				->select(' 1 as StatusHorario, TIME_FORMAT(horaApertura,"%h-%i %p") AS HoraA,TIME_FORMAT(horaCierre,"%h-%i %p") AS HoraC')
				->where("$hora BETWEEN horaApertura and horaCierre and status = 1 and iddias = $dia and idEstablecimiento = $id")
                ->fetch();
                                
			if(is_object($horario)){
                if($horario->StatusHorario == 1){
                    $id_establecimiento = $data['idEstablecimiento'];
                    $coordenadas_establecimiento = $this->db->from($this->tbEstablecimiento)
                        ->select(null)
                        ->select('
                            direccion.idDireccion, 
                            direccion.Latitud, 
                            direccion.Longitud, 
                            CONCAT(direccion.Calle, " ", direccion.numeroExterior, " ", direccion.Colonia, " ", direccion.Municipio) AS DireccionEstablecimiento, 
                            IFNULL(establecimiento.Nombre, "Sin detalle") AS Nombre, 
                            IFNULL(establecimiento.urlFoto, "Sin detalle") AS urlFoto,
                            IFNULL(establecimiento.telefono, "Sin detalle") AS telefono,
                            establecimiento.idCiudad,
                            ciudad.CostoEnvio
                        ')
                        ->leftJoin('direccion ON direccion.idEstablecimiento = establecimiento.idEstablecimiento')
                        ->leftJoin('ciudad ON ciudad.idCiudad = establecimiento.idCiudad')
                        ->where('establecimiento.idEstablecimiento', $id_establecimiento)
                        ->fetch();
                    
                    $latitud_origen = $coordenadas_establecimiento->Latitud;
                    $longitud_origen = $coordenadas_establecimiento->Longitud;
                    $id_origen = floatval($coordenadas_establecimiento->idDireccion);
                    $id_ciudad = floatval($coordenadas_establecimiento->idCiudad);
                    $costo_envio = floatval($coordenadas_establecimiento->CostoEnvio);
                    $direccion_establecimiento = $coordenadas_establecimiento->DireccionEstablecimiento;
                }else{
                           $this->response->errors = 'El establecimiento se encuentra cerrado';
                    return $this->response->SetResponse(false);
                }
            }else{
                       $this->response->errors = 'El establecimiento se encuentra cerrado';
                return $this->response->SetResponse(false);
            }
            //FIN 
        } else {
            $id_origen = $data['idDireccionOrigen'];
            $coordenadas_direccion = $this->db->from($this->tbDireccion)
                ->select(null)
                ->select('
                    Latitud, 
                    Longitud,
                    CONCAT(direccion.Calle, " ", direccion.numeroExterior, " ", direccion.Colonia, " ", direccion.Municipio) AS DireccionEstablecimiento
                ')
                ->where('idDireccion', $id_origen)
                ->fetch();

            $latitud_origen = $coordenadas_direccion->Latitud;
            $longitud_origen = $coordenadas_direccion->Longitud;
            $direccion_establecimiento = $coordenadas_direccion->DireccionEstablecimiento;
        }
        
        $repartidores = RestApi::call(RestApiMethod::GET, 'Repartidor/'.$id_ciudad.'.json?orderBy="activo"&equalTo=1', []);
            
        if ($repartidores != false) {
            #Actualizar Lat-Long
            foreach ($repartidores as $key => $value) {
                $consultar_repartidor = $this->db->from($this->tbCoordenada)
                    ->select(null)
                    ->select('idCoordenada')
                    ->where('idRepartidor', $key)
                    ->fetch();
                    
                if ($consultar_repartidor != false) {
                    $actualizar_coordenadas = $this->db->update($this->tbCoordenada, ['Latitud' => $value->latitud, 'Longitud' => $value->longitud])
                        ->where('idRepartidor', $key)
                        ->execute();
                } else {
                    $registrar_coordenadas = $this->db->insertInto($this->tbCoordenada, ['Latitud' => $value->latitud, 'Longitud' => $value->longitud, 'idRepartidor' => $key])
                        ->execute();
                }
            }
            
            $repartidor_mas_cercano = $this->db->from($this->tbCoordenada)
                ->select(null)
                ->select(
                    '(6371 * ACOS(COS(RADIANS('.$latitud_origen.')) * COS(RADIANS(Latitud)) * COS(RADIANS(Longitud) - RADIANS('.$longitud_origen.')) + SIN(RADIANS('.$latitud_origen.')) *SIN(RADIANS(Latitud)))) AS distance, 
                    coordenada.idRepartidor,
                    IFNULL(persona.nombre, "Sin detalle") AS nombre,
                    IFNULL(persona.telefono, "Sin detalle") AS telefono,
                    IFNULL(tokenPush.token, "Sin detalle") AS token,
                    IFNULL(persona.urlFoto,"Sin detalle") AS urlFoto
                ')
                // (SELECT COUNT(*) FROM orden WHERE (orden.idRepartidor = coordenada.idRepartidor) AND (orden.idStatusOrden < 6)) AS TotalOrdenesActivas
                ->leftJoin('persona ON persona.idUsuario = coordenada.idRepartidor')
                ->leftJoin('tokenPush ON tokenPush.idUsuario = persona.idUsuario')
                ->leftJoin('orden ON orden.idRepartidor = coordenada.idRepartidor')
                ->where('persona.idTipoUsuario = 2 AND persona.idStatusPersona = 1 AND persona.activo = 1')
                ->where('persona.idCiudad', $id_ciudad)
                ->having('distance > 0 AND distance < 5')
                ->groupBy('idRepartidor')
                // ->orderBy('TotalOrdenesActivas ASC')
                ->limit(1)
                ->fetchAll();

            if (count($repartidor_mas_cercano) > 0) {
                $id_repartidor_mas_cercano = $repartidor_mas_cercano[0]->idRepartidor;
                $token_repartidor = $repartidor_mas_cercano[0]->token;

                $direccion_destino = $this->db->from($this->tbDireccion)
                ->select(null)
                ->select('
                    Latitud, 
                    Longitud,
                    TextDir AS DireccionEntrega
                ')
                ->where('idDireccion', $id_destino)
                ->fetch();
                $id_usuario = $data['idUsuario'];
                // telefono del cliente
                $buscartelefonoCliente = $this->db->from($this->tbPersona)
                    ->select(null)
                    ->select('telefono, tokenPush.token')
                    ->leftJoin('tokenPush ON tokenPush.idUsuario = persona.idUsuario')
                    ->where('persona.idUsuario', $id_usuario)
                    ->fetch();

                $telefonoCliente = $buscartelefonoCliente->telefono;
                $token_usuario = $buscartelefonoCliente->token;

                if ($data['NombreCliente'] != "") {
                    $nombre_recibe = $data['NombreCliente'];
                } else {
                    $cliente_recibe = $this->db->from($this->tbPersona)
                                               ->select(null)
                                               ->select('CONCAT(nombre, " ", apellidos) AS NombreRecibe')
                                               ->where('idUsuario', $id_usuario)
                                               ->fetch();
                    
                    $nombre_recibe = $cliente_recibe->NombreRecibe;
                }

                $informacion_orden = [
                    'idUsuario' => $id_usuario,
                    'NombreRecibe' => $nombre_recibe,
                    'idRepartidor' => floatval($id_repartidor_mas_cercano),
                    'idEstablecimiento' => $id_establecimiento, 
                    'idDireccionOrigen' => $id_origen,
                    'idDireccionDestino' => floatval($id_destino),
                    'PagaCon' => $data['PagareCon'],
                    'Descripcion' => $data['Descripcion'],
                    'CostoEnvio' => $costo_envio
                ];

                $registrar_orden = $this->altaOrdenMySql($informacion_orden);
                
                if ($registrar_orden > 0) {
                    $fechaActual = date('Y-m-d H:i:s');
                    $registrar_orden_firebase = RestApi::call(RestApiMethod::POST, "Orden/$registrar_orden.json", []);
                    $actualizar_informacion_orden = RestApi::call(RestApiMethod::PUT, "Orden/$registrar_orden.json", 
                    [
                        'NombreRecibe' => $nombre_recibe,
                        'NombreRepartidor' => $repartidor_mas_cercano[0]->nombre,
                        'TelefonoRepartidor' => $repartidor_mas_cercano[0]->telefono,
                        'FotoRepartidor' =>$repartidor_mas_cercano[0]->urlFoto,
                        'TelefonoCliente' => $telefonoCliente,
                        'idRepartidor' => floatval($id_repartidor_mas_cercano),
                        'idUsuario' => $id_usuario,
                        'LatitudEstablecimiento' => $latitud_origen,
                        'LongitudEstablecimiento' => $longitud_origen,
                        'DireccionEstablecimiento' => $direccion_establecimiento,
                        'NombreEstablecimiento' => $coordenadas_establecimiento->Nombre,
                        'FotoEstablecimiento' => $coordenadas_establecimiento->urlFoto,
                        'TelefonoEstablecimiento' => $coordenadas_establecimiento->telefono,
                        'DireccionDestino' => $direccion_destino->DireccionEntrega,
                        'LatitudDestino' => $direccion_destino->Latitud,
                        'LongitudDestino' => $direccion_destino->Longitud,
                        'Descripcion' => $data['Descripcion'],
                        'PagaCon' => $data['PagareCon'],
                        'StatusOrden' => 1,
                        'CostoEnvio' => $costo_envio,
                        'idOrden' => $registrar_orden,
                        'horaStatus'=>[
                            "Recibida"          =>[
                                                    "Fecha"=>"",
                                                    "Hora"=>""
                                                ],
                            "AtendiendoOrden"   =>[
                                                    "Fecha"=>"",
                                                    "Hora"=>""
                                                ],
                            "EnEstablecimiento" =>[
                                                    "Fecha"=>"",
                                                    "Hora"=>""
                                                ],
                            "EnCamino"          =>[
                                                    "Fecha"=>"",
                                                    "Hora"=>""
                                                ],
                            "EnLugarDeEntrega"  =>[
                                                    "Fecha"=>"",
                                                    "Hora"=>""
                                                ],
                            "Entregada"         =>[
                                                    "Fecha"=>"",
                                                    "Hora"=>""
                                                ]
                        ]
                    ]);
                    // add fecha de peticion
                    $this->addFechaStatusOrden($registrar_orden,1);
                    $obtener_ordenes_activas = RestApi::call(RestApiMethod::GET, "Repartidor/$id_ciudad/$id_repartidor_mas_cercano/ordenes.json", []);
                    
                    if ($obtener_ordenes_activas == "null") {
                        $agregar_orden = RestApi::call(RestApiMethod::POST, "Repartidor/$id_ciudad/$id_repartidor_mas_cercano/ordenes.json", []);
                        $actualizar_orden = RestApi::call(RestApiMethod::PUT, "Repartidor/$id_ciudad/$id_repartidor_mas_cercano/ordenes.json", [$registrar_orden]);
                    } else {
                        $ordenes_activas = array_push($obtener_ordenes_activas, $registrar_orden);
                        $actualizar_orden = RestApi::call(RestApiMethod::PUT, "Repartidor/$id_ciudad/$id_repartidor_mas_cercano/ordenes.json", $obtener_ordenes_activas);
                    }

                    $notificar_repartidor = Push::FMC('Nueva orden 📦', 'Te ha sido asignado un nuevo pedido, revísalo en tu lista de pedidos pendientes', $token_repartidor,1, 'Default');
                    $notificar_usuario = Push::FMC('Orden recibida', 'Tu orden ha sido recibida y asignada a uno de nuestros repartidores', $token_usuario,'', 'Default');
                        // add carrito
                        foreach ($data["Carrito"] as $value) {
                            $value["idOrden"] = $registrar_orden;
                            $registerCarrito = $this->db->insertInto($this->tbTrolley, $value)
                                    ->execute();
                        }
                        // 
                        //        $this->response->result = $registerCarrito;
                        // return $this->response->SetResponse(true,'Agregado con exito');
                           $this->response->result = $registrar_orden;
                    return $this->response->SetResponse(true, 'Orden registrada y asignada a uno de nuestros repartidores.');
                } else {
                           $this->response->errors = 'No se pudo registrar la orden';
                    return $this->response->SetResponse(false);
                }
            } else {
                       $this->response->errors = 'No hay repartidores cerca.';
                return $this->response->SetResponse(false);
            }
        } else {
                   $this->response->errors = 'No hay repartidores disponibles.';
            return $this->response->SetResponse(false);
        }
    }

    public function altaOrdenMySql($data){
        $registrar_orden = $this->db->insertInto($this->tbOrden, $data)
                                    ->execute();
        return $registrar_orden;
    }

    public function detail($idOrder){
        $order_detail = $this->db->from($this->tbOrden)
            ->select(null)
            ->select('
                orden.idOrden, 
                IFNULL(establecimiento.nombre, "Sin detalle") AS NombreEstablecimiento,
                IFNULL(statusorden.Descripcion, "Sin detalle") AS StatusOrden,
                IFNULL(persona.nombre, "Sin detalle") AS NombreRepartidor,
                IFNULL(direccion.Latitud, "Sin detalle") AS LatitudOrigen,
                IFNULL(direccion.Longitud, "Sin detalle") AS LongitudOrigen,
                IFNULL(direccion.Latitud, "Sin detalle") AS LatitudDestino,
                IFNULL(direccion.Longitud, "Sin detalle") AS LongitudDestino,
                orden.Descripcion AS DescripcionOrden,
                IFNULL(persona.urlFoto, "Sin detalle") AS FotoRepartidor,
                IFNULL(statusorden.idStatusOrden, "Sin detalle") AS idStatusOrden,
                IFNULL(establecimiento.calificacion, "Sin detalle") AS CalificacionEstablecimiento,
                IFNULL(establecimiento.urlFoto, "Sin detalle") AS FotoEstablecimiento')
            ->leftJoin('establecimiento on establecimiento.idEstablecimiento = orden.idEstablecimiento')
            ->leftJoin('statusorden on statusorden.idStatusOrden = orden.idStatusOrden')
            ->leftJoin('persona on persona.idUsuario = orden.idUsuario')
            ->leftJoin('direccion on direccion.idDireccion = orden.idDireccionOrigen')
            ->where('idOrden', $idOrder)
            ->fetch();
        
               $this->response->result = $order_detail;
    	return $this->response->SetResponse(true);
    }

    public function list($id_usuario, $limit, $offset, $statusOrder){
        $where = "";
        if ($statusOrder == 1) { //asignación del valor statusOrder = 1 activo
            $where = "orden.idStatusOrden < 6 AND (orden.idUsuario = $id_usuario OR orden.idRepartidor = $id_usuario)";
        } elseif ($statusOrder == 0) { //asignación del valor statusOrder = 0 inactivo
            $where = "orden.idStatusOrden >= 6 AND (orden.idUsuario = $id_usuario OR orden.idRepartidor = $id_usuario)";
        }

        $data = $this->db->from($this->tbOrden)
                        ->select(null) 
                        ->select('
                            orden.idOrden, IFNULL(establecimiento.nombre,"sin detalle") AS NombreEstablecimiento,
                            IFNULL(establecimiento.urlFoto, "sin detalle") AS FotoEstablecimiento,
                            orden.descripcion AS Descripcion,
                            IFNULL(persona.nombre, "sin detalle") AS NombreRepartidor,
                            IFNULL(statusorden.Descripcion, "sin detalle") AS StatusOrden,
                            IFNULL(establecimiento.calificacion, "Sin detalle") AS CalificacionEstablecimiento, 
                            CONCAT(
                                direccionOrigen.Calle, " ", 
                                direccionOrigen.numeroExterior, " ", 
                                direccionOrigen.numeroInterior, " ", 
                                direccionOrigen.Colonia, " ", 
                                direccionOrigen.Municipio
                            ) AS Direccion,
                            orden.idStatusOrden,
                            DATE_FORMAT(FechaPeticion,"%d/%m/%Y %h;%i") as FechaPeticion,
                            persona.urlFoto AS FotoRepartidor,
                            direccionOrigen.Latitud AS LatitudOrigen,
                            direccionOrigen.Longitud AS LongitudOrigen,
                            direccionDestino.Latitud AS LatitudDestino,
                            direccionDestino.Longitud AS LongitudDestino,
                            persona.telefono AS TelefonoRepartidor
                            '
                        )
                        ->leftJoin('establecimiento on establecimiento.idEstablecimiento = orden.idEstablecimiento')
                        ->leftJoin('persona on persona.idUsuario = orden.idRepartidor')
                        ->leftJoin('statusorden on statusorden.idStatusOrden = orden.idStatusOrden')
                        ->leftJoin ('direccion AS direccionOrigen on direccionOrigen.idDireccion = orden.idDireccionOrigen')
                        ->leftJoin ('direccion AS direccionDestino on direccionDestino.idDireccion = orden.idDireccionDestino')
                        ->where($where)
                        ->limit($limit)
                        ->offset($offset)
                        ->orderBy('orden.idOrden DESC')
                        ->fetchAll();

        if ($data != false) {
            $total_ordenes = $this->db->from($this->tbOrden)
                ->select("COUNT(*) AS total") 
                ->leftJoin('establecimiento on establecimiento.idEstablecimiento = orden.idEstablecimiento')
                ->leftJoin('persona on persona.idUsuario = orden.idRepartidor')
                ->leftJoin('statusorden on statusorden.idStatusOrden = orden.idStatusOrden')
                ->leftJoin ('direccion on direccion.idDireccion = orden.idDireccionOrigen')
                ->where($where)
                ->fetch()
                ->total;

                   $this->response->result= [
                       "data" => $data,
                       "total" => $total_ordenes
                   ];
		    return $this->response->SetResponse(true);
        } else {
                   $this->response->errors = 'Sin registro de órdenes';
            return $this->response->SetResponse(false);
        }
    }

    public function reporteOrdenes($fechaIni,$fechaFin,$statusOrder){
        $where = "";
        if ($statusOrder == 1) { //asignación del valor statusOrder = 1 activo
            $where = "orden.idStatusOrden < 6 AND (FechaPeticion BETWEEN '$fechaIni 00:00:00' And '$fechaFin 23:59:59')";
        } elseif ($statusOrder == 0) { //asignación del valor statusOrder = 0 inactivo
            $where = "orden.idStatusOrden = 6 AND (FechaPeticion BETWEEN '$fechaIni 00:00:00' And '$fechaFin 23:59:59')";
        }elseif($statusOrder == 400){
            $where = "orden.idStatusOrden = 400 AND (FechaPeticion BETWEEN '$fechaIni 00:00:00' And '$fechaFin 23:59:59') ";
        }

        $data = $this->db->from($this->tbOrden)
                        ->select(null) 
                        ->select('
                            orden.idOrden, IFNULL(establecimiento.nombre,"sin detalle") AS NombreEstablecimiento,
                            IFNULL(establecimiento.urlFoto, "sin detalle") AS FotoEstablecimiento,
                            orden.descripcion AS Descripcion,
                            IFNULL(persona.nombre, "sin detalle") AS NombreRepartidor,
                            IFNULL(statusorden.Descripcion, "sin detalle") AS StatusOrden,
                            IFNULL(establecimiento.calificacion, "Sin detalle") AS CalificacionEstablecimiento, 
                            CONCAT(
                                direccionOrigen.Calle, " ", 
                                direccionOrigen.numeroExterior, " ", 
                                direccionOrigen.numeroInterior, " ", 
                                direccionOrigen.Colonia, " ", 
                                direccionOrigen.Municipio
                            ) AS Direccion,
                            orden.idStatusOrden,
                            persona.urlFoto AS FotoRepartidor,
                            direccionOrigen.Latitud AS LatitudOrigen,
                            direccionOrigen.Longitud AS LongitudOrigen,
                            direccionDestino.Latitud AS LatitudDestino,
                            direccionDestino.Longitud AS LongitudDestino,
                            persona.telefono AS TelefonoRepartidor,
                            orden.NombreRecibe,
                            orden.CostoEnvio,
                            orden.FechaPeticion,
                            statusorden.Descripcion AS StatusOrden
                            '
                        )
                        ->leftJoin('establecimiento on establecimiento.idEstablecimiento = orden.idEstablecimiento')
                        ->leftJoin('persona on persona.idUsuario = orden.idRepartidor')
                        ->leftJoin('statusorden on statusorden.idStatusOrden = orden.idStatusOrden')
                        ->leftJoin ('direccion AS direccionOrigen on direccionOrigen.idDireccion = orden.idDireccionOrigen')
                        ->leftJoin ('direccion AS direccionDestino on direccionDestino.idDireccion = orden.idDireccionDestino')
                        ->leftJoin('statusorden on statusorden.idStatusOrden = orden.idStatusOrden')
                        ->where($where)
                        // ->limit($limit)
                        // ->offset($offset)
                        ->orderBy('orden.idOrden DESC')
                        ->fetchAll();

        if ($data != false) {
            $MontoTotal = $this->db->from($this->tbOrden)
                        ->select('sum(orden.CostoEnvio) AS MontoTotal')
                        ->leftJoin('establecimiento on establecimiento.idEstablecimiento = orden.idEstablecimiento')
                        ->leftJoin('persona on persona.idUsuario = orden.idRepartidor')
                        ->leftJoin('statusorden on statusorden.idStatusOrden = orden.idStatusOrden')
                        ->leftJoin ('direccion AS direccionOrigen on direccionOrigen.idDireccion = orden.idDireccionOrigen')
                        ->leftJoin ('direccion AS direccionDestino on direccionDestino.idDireccion = orden.idDireccionDestino')
                        ->where($where)
                        // ->limit($limit)
                        // ->offset($offset)
                        ->orderBy('orden.idOrden DESC')
                        ->fetch()
                        ->MontoTotal;



            // $total_ordenes = $this->db->from($this->tbOrden)
            //     ->select("COUNT(*) AS total") 
            //     ->leftJoin('establecimiento on establecimiento.idEstablecimiento = orden.idEstablecimiento')
            //     ->leftJoin('persona on persona.idUsuario = orden.idRepartidor')
            //     ->leftJoin('statusorden on statusorden.idStatusOrden = orden.idStatusOrden')
            //     ->leftJoin ('direccion on direccion.idDireccion = orden.idDireccionOrigen')
            //     ->where($where)
            //     ->fetch()
            //     ->total;

                   $this->response->result= [
                       "data" => $data,
                       "montoTotal" => $MontoTotal
                    //    ,"total" => $total_ordenes
                   ];
		    return $this->response->SetResponse(true);
        } else {
                   $this->response->errors = 'Sin registro de órdenes';
            return $this->response->SetResponse(false);
        }
    }

    public function finish(){
        $this->response->result=120;
        return $this->response->SetResponse(true,'Orden Finalizada');
    }

    public function detailDealer($idOrder){
        $detail_dealer = $this->db->from($this->tbOrden)
        ->select(null)
        ->select('
            orden.idOrden, 
            IFNULL(statusorden.Descripcion, "Sin detalle") AS StatusOrden,
            IFNULL(establecimiento.nombre, "Sin detalle") AS NombreEstablecimiento,
            CONCAT(
                Origen.Calle, " ", 
                Origen.numeroExterior, " ", 
                Origen.numeroInterior, " ", 
                Origen.Colonia, " ", 
                Origen.Municipio, " "
            ) AS Direccion,
            IFNULL(establecimiento.telefono, "Sin detalle") AS TelefonoEstablecimiento,
            IFNULL(persona.telefono, "Sin detalle") AS TelefonoUsuario,
            orden.NombreRecibe AS NombreEntregarA,
            orden.PagaCon AS PagaCon,
            IFNULL(Origen.Latitud, "Sin detalle") AS LatitudOrigen,
            IFNULL(Origen.Longitud, "Sin detalle") AS LongitudOrigen,
            IFNULL(Destino.Latitud, "Sin detalle") AS LatitudDestino,
            IFNULL(Destino.Longitud, "Sin detalle") AS LongitudDestino,
            orden.Descripcion AS DescripcionOrden')
        ->leftJoin('establecimiento on establecimiento.idEstablecimiento = orden.idEstablecimiento')
        ->leftJoin('statusorden on statusorden.idStatusOrden = orden.idStatusOrden')
        ->leftJoin('persona on persona.idUsuario = orden.idUsuario')
        ->leftJoin('direccion As Origen on Origen.idDireccion = orden.idDireccionOrigen')
        ->leftJoin('direccion As Destino on Destino.idDireccion = orden.idDireccionDestino')
        ->where('idOrden', $idOrder)
        ->fetch();
    
        $this->response->result = $detail_dealer;
        return $this->response->SetResponse(true);
    }

    public function updateStatus($idOrder, $status, $data){
        if ($status > 1 && $status < 7 || $status == 400) {
            $obtener_orden = $this->db->from($this->tbOrden)
                ->select(null)
                ->select('orden.idUsuario, orden.idRepartidor, tokenUsuario.token AS TokenUsuario, tokenRepartidor.token AS TokenRepartidor')
                ->leftJoin('tokenPush AS tokenUsuario ON tokenUsuario.idUsuario = orden.idUsuario')
                ->leftJoin('tokenPush AS tokenRepartidor ON tokenRepartidor.idUsuario = orden.idRepartidor')
                ->where('idOrden', $idOrder)
                ->fetch();
            
            if ($obtener_orden != false) {
                $token_usuario = $obtener_orden->TokenUsuario;
                $token_repartidor = $obtener_orden->TokenRepartidor;

                $actualizar_status_mysql = $this->db->update($this->tbOrden)
                    ->set('idStatusOrden', $status)
                    ->where('idOrden', $idOrder)
                    ->execute();
                
                $actualizar_status_firebase = RestApi::call(RestApiMethod::PUT, "Orden/$idOrder/StatusOrden.json", floatval($status));

                $titulo_push_usuario = '';
                $mensaje_push_usuario = '';
                // add addFechaStatusOrden
                $this->addFechaStatusOrden($idOrder,$status);
                switch ($status) {
                    case 2: 
                        $titulo_push_usuario = 'Vamos por tu pedido'; 
                        $mensaje_push_usuario = 'El repartidor está en camino para recoger tu pedido';
                        break;
                    case 3: 
                        $titulo_push_usuario = 'Llegamos por tu pedido'; 
                        $mensaje_push_usuario = 'Nuestro repartidor está en el establecimiento para recoger tu orden';
                        break;
                    case 4: 
                        $titulo_push_usuario = '¡En camino!'; 
                        $mensaje_push_usuario = 'Ya tenemos tu pedido y vamos en camino a entregártelo :guiño:';
                        break;
                    case 5: 
                        $titulo_push_usuario = '¡Tu pedido ha llegado!'; 
                        $mensaje_push_usuario = 'Encuentra al repartidor en el lugar de entrega';
                        break;
                    case 6: 
                        $titulo_push_usuario = '¡Entregamos tu pedido!'; 
                        $mensaje_push_usuario = 'Ya entregamos tu pedido, disfruta tu comida 😉';
                        //add boleto
                        // $this->addBoleto($idOrder);
                        //
                        // eliminar orden de firebase y de lista de repartidor de firebase
                        break;

                    case 400:
                        $titulo_push_usuario = 'Pedido cancelado';
                        $mensaje_push_usuario = 'Lo sentimos mucho, tu pedido ha sido cancelado';

                        $agregar_cancelacion = $this->db->insertInto($this->tbCancelarPedido, 
                            [
                                'Descripcion' => $data['Descripcion'], 'idOrden' => $idOrder
                            ])
                            ->execute();
                        // eliminar orden de firebase y de lista de repartidor de firebase
                        break;
                        // agregar campo de fecha de status
                        
                }

                $notificar_usuario = Push::FMC($titulo_push_usuario, $mensaje_push_usuario, $token_usuario, '', 'Default');

                    $this->response->result= ([
                        "idOrden" => floatval($idOrder),
                        "idStatus" => floatval($status)
                    ]);
                return $this->response->SetResponse(true);
            } else {
                       $this->response->errors='Tenemos problemas al obtener información de tu orden';
                return $this->response->SetResponse(false);
            }
        } else {
                   $this->response->errors='Estatus no válido';
            return $this->response->SetResponse(false);
        }
    }
    // addBoleto
    public function addBoleto($idOrden){
        // bucar id User
        // saber si esta registrado en boletos
        $datosUser = $this->db->from($this->tbOrden)
                              ->select(null)
                              ->select("orden.idUsuario, ifnull(numOrdenes,'no hay registro') as numOrdenes")
                              ->leftJoin("countOrdenes on countOrdenes.idUsuario = orden.idUsuario")
                              ->where("idOrden",$idOrden)
                              ->fetch();
        // registrar si no esta
        $numeroOrdenes = 0;
        if($datosUser->numOrdenes == "no hay registro"){
            // registrar en tabla
            $numeroOrdenes = 1; 
            $altaCountOrdenes = $this->db->insertInto($this->tbcountOrdenes,['idUsuario'=>$datosUser->idUsuario,'numOrdenes'=>$numeroOrdenes])
                                         ->execute();;
        }else{
            $numeroOrdenes = $datosUser->numOrdenes;
            $numeroOrdenes++;
            // actualizar numero de ordenes
            $actualizar_num_ordenes = $this->db->update($this->tbcountOrdenes, ['numOrdenes' => $numeroOrdenes])
                        ->where('idUsuario', $datosUser->idUsuario)
                        ->execute();
            if($numeroOrdenes == 3){
                // select idOrden from orden where idUsuario = 24 and idStatusOrden = 6 order by idOrden desc limit 3
                // craer boleto
                $altaBoleto = $this->db->insertInto('boleto', ['idUsuario'=>$datosUser->idUsuario])
                                       ->execute();
                // agregar detalles del boleto
                // obtener los tres boletos q forman el boleto  
                $infoDetalleBoleto = $this->db->from($this->tbOrden)
                                              ->select(null)
                                              ->select('idOrden')
                                              ->where('idUsuario = :idUsuario and idStatusOrden = 6',[":idUsuario"=>$datosUser->idUsuario])
                                              ->orderBy('idOrden desc')
                                              ->limit(3)
                                              ->fetchAll();
                foreach ($infoDetalleBoleto as $value) {
                    $detallesBoleto = $this->db->insertInto('detalleBoleto',['idboleto'=>$altaBoleto,'idOrden'=>$value->idOrden])
                                               ->execute();
                }
                // llevar a 0 el numero de ordenes
                $actualizar_num_ordenes = $this->db->update($this->tbcountOrdenes, ['numOrdenes' => 0])
                        ->where('idUsuario', $datosUser->idUsuario)
                        ->execute();
            }
        }
    }
    // addFecaHora de status de ordenes
    public function addFechaStatusOrden($idOrden,$idStatus){
        $fechaActual = date('Y-m-d H:i:s');
        $fecha = date('d/m/Y');
        $hora = date('H:i');
        $altaFechaStatus = $this->db->insertInto("detalleStatusOrden",["idOrden"=>$idOrden,"idStatusOrden"=>$idStatus,"Fecha"=>$fechaActual])
                                    ->execute();
        // firebase
        $campo = $idStatus;
        switch ($idStatus) {
            case 1:
                $campo = "Recibida";
                break;
            case 2: 
                $campo = "AtendiendoOrden";
                break;
            case 3: 
                $campo = "EnEstablecimiento";
                break;
            case 4: 
                $campo = "EnCamino";
                break;
            case 5: 
                $campo = "EnLugarDeEntrega";
                break;
            case 6: 
                $campo = "Entregada";
                break;           
        }
        // 
        RestApi::call(
            RestApiMethod::PATCH,
            "Orden/$idOrden/horaStatus.json",
            [
                "$campo"    => [
                                    "Fecha"=>$fecha,
                                    "Hora"=>$hora
                               ]
            ]
        );
        return $altaFechaStatus;
    }
    // 
    public function push($id){
        $token = $this->db->from("tokenPush")
        ->select('token')
        // ->select('orden.idUsuario, orden.idRepartidor, tokenUsuario.token AS TokenUsuario, tokenRepartidor.token AS TokenRepartidor')
        // ->leftJoin('tokenPush AS tokenUsuario ON tokenUsuario.idUsuario = orden.idUsuario')
        // ->leftJoin('tokenPush AS tokenRepartidor ON tokenRepartidor.idUsuario = orden.idRepartidor')
        ->where('idUsuario', $id)
        ->fetch()
        ->token;
        $notificaccion = Push::FMC("prueba","sigue al conejo blanco", $token,1, 'Default');

            $this->response->result= $notificaccion;
        return $this->response->SetResponse(true);

    }

    public function reportDashboard($de,$a)
    {
        $data = $this->db->from($this->tbOrden)
                         ->select(null)
                         ->select(
                                 'COUNT(orden.idOrden) AS NumeroOrdenes,
                                  IFNULL(sum(orden.CostoEnvio), "0") AS Ganancias')
                         ->where('idStatusOrden = 6')
                         ->where("FechaPeticion BETWEEN '$de 00:00:00' AND '$a 23:59:59'", 
                           array(':de' => $de , ':a' => $a ) )
                         ->orderBy('idOrden DESC')
                         ->fetch();

        if	($data !=false)	{
                    $this->response->result= $data;
            return $this->response->SetResponse(true);
        }else{
                    $this->response->errors='Sin registros de Reportes';
            return $this->response->SetResponse(false);
        }
    }

    public function reportDashboardUser($de,$a)
    {
        $data = $this->db->from($this->tbPersona)
                         ->select(null)
                         ->select('
                                  IFNULL(sum(persona.idTipoUsuario = 1),"0") AS NumUsuarios,
                                  IFNULL(sum(persona.idTipoUsuario = 2), "0") AS NumRepartidores')
                         ->where('idStatusPersona = 1')
                         ->where("FechaRegistro BETWEEN '$de 00:00:00' AND '$a 23:59:59'", 
                           array(':de' => $de , ':a' => $a ) )
                         ->orderBy('idUsuario DESC')
                         ->fetch();
                         
        if	($data !=false)	{
                    $this->response->result= $data;
            return $this->response->SetResponse(true);
        }else{
                    $this->response->errors='Sin registros de Reportes';
            return $this->response->SetResponse(false);
        }
    }

}