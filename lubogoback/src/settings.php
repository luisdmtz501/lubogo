<?php
return [
    'settings' => [
        'displayErrorDetails' => true,

        // Renderer settings
        'renderer' => [
            'template_path' => __DIR__ . '/../templates/',
        ],

        // Monolog settings
        'logger' => [
            'name' => 'slim-app',
            'path' => __DIR__ . '/../logs/app.log',
        ],

        // Configuración de mi APP
        'app_token_name'   => 'auth-token',
        'connectionString' => [
            // 'dns'  => 'mysql:host=localhost;dbname=lubogo;charset=utf8', # dev and local #3.82.157.59
            // 'dns'  => 'mysql:host=3.82.157.59;dbname=lubogo;charset=utf8', #dev aws
            // 'dns'  => 'mysql:host=54.174.116.121;dbname=lubogo;charset=utf8', #prod aws
            'dns'  => 'mysql:host=localhost;dbname=u219376423_lubogo;charset=utf8', #prod hostinguer
            // 'dns'  => 'mysql:host=localhost;dbname=u219376423_lubogodev;charset=utf8', #dev hostinguer
            // 'user' => 'LuboGo',
            'user'=>'u219376423_LuboGo',#pro hostinguer
            // 'user'  => 'u219376423_LuboGodev',#dev hostinguer
            'pass' => 'Stardust88/@LuboGo*'

        ]
    ],
];
